package com.Material;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;


public class Utilities {
	public static Thread updateThread;
	public static void startAnimationThreadStuff(long delay,
			final CircularProgressView progressView) {
		if (updateThread != null && updateThread.isAlive())
			updateThread.interrupt();
		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				// Start animation after a delay so there's no missed frames
				// while the app loads up
				progressView.setProgress((int) 0f);
				progressView.startAnimation(); // Alias for resetAnimation, it's
												// all the same
				// Run thread to update progress every half second until full
				updateThread = new Thread(new Runnable() {
					@Override
					public void run() {
						while (progressView.getProgress() < progressView
								.getMaxProgress() && !Thread.interrupted()) {
							// Must set progress in UI thread
							handler.post(new Runnable() {
								@Override
								public void run() {
									progressView.setProgress(progressView
											.getProgress() + 10);
								}
							});
							SystemClock.sleep(250);
						}
					}
				});
				updateThread.start();
			}
		}, delay);
	}
	
	public static String readJson(String getURL) {
		StringBuilder builder = new StringBuilder();

		try {
			HttpParams httpParameters = new BasicHttpParams();
			int timeoutConnection = 20000;
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					timeoutConnection);
			int timeoutSocket = 20000;
			HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

			HttpClient client = new DefaultHttpClient(httpParameters);
			HttpPost httpPost = new HttpPost(getURL);
			HttpResponse response = client.execute(httpPost);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} else {
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}

}
