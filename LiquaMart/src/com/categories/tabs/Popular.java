package com.categories.tabs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

import com.Material.CircularProgressView;
import com.Material.ConnectionDetector;
import com.Material.PullRefreshLayout;
import com.liquamart.Question;
import com.liquamart.R;
import com.products.MyProductSetter;
import com.products.ProductAdapter;

public class Popular extends Fragment {
	private FragmentActivity activity;
	SwipeRefreshLayout mSwipeRefreshLayout;
	Typeface type;
	ListView listView;
	List<MyProductSetter> rowItems;
	public String[] names;
	public String[] imageurls;
	public String[] descriptions;
	public String[] productids;
	public Boolean[] popular;
	Boolean isInternetPresent = false;
	ConnectionDetector cd;
	PullRefreshLayout layout;
	 @Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			activity = getActivity();
			setRetainInstance(true);
		}
	 
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
    	
        View rootView = inflater.inflate(R.layout.latestproducts, container, false);
        cd = new ConnectionDetector(activity);
        type = Typeface.createFromAsset(activity.getAssets(),
				"fonts/sanfracisco_regular.ttf");
        listView = (ListView) rootView.findViewById(R.id.list);
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
        new GetProductsLatest().execute();
        }
//		mSwipeRefreshLayout=(SwipeRefreshLayout)rootView.findViewById(R.id.swipeRefreshLayout);
//		mSwipeRefreshLayout.setColorSchemeResources(R.color.blue,R.color.orange,R.color.green);
//		mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {  
//		    @Override
//		    public void onRefresh() {
//		        // Refresh items
//		    	isInternetPresent = cd.isConnectingToInternet();
//                if (isInternetPresent) {
//		        new GetProductsLatest().execute();
//		        mSwipeRefreshLayout.setRefreshing(false);
//                }
//                else
//                {
//                	showAlertDialog(activity, "No Internet Connection",
//                            "You don't have internet connection.", false);
//                }
//		    }
//		});
        layout = (PullRefreshLayout)rootView.findViewById(R.id.swipeRefreshLayout);
		  layout.setRefreshStyle(PullRefreshLayout.STYLE_WATER_DROP);
		// listen refresh event
		layout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
		    @Override
		    public void onRefresh() {
		        // start refresh
		    	isInternetPresent = cd.isConnectingToInternet();
		        if (isInternetPresent) {
		        new GetProductsLatest().execute();
		        layout.setRefreshing(false);
		        }
		        else
		        {
		        	showAlertDialog(activity, "No Internet Connection",
		                    "You don't have internet connection.", false);
		        	layout.setRefreshing(false);
		        }
		        
		    }
		});
         
        return rootView;
    }
    
    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
 
        // Setting Dialog Title
        alertDialog.setTitle(title);
 
        // Setting Dialog Message
        alertDialog.setMessage(message);
         
        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.app_icon : R.drawable.app_icon);
 
        // Showing Alert Message
        alertDialog.show();
    }
    
    private class GetProductsLatest extends AsyncTask<String, String, String> {
		Dialog dialog;
		CircularProgressView progressView;
		TextView progress_text;
		String result, id,latestproducts;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// custom dialog
//			dialog = new Dialog(activity);
//			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//			dialog.setContentView(R.layout.custom_dialog);
//			dialog.setCanceledOnTouchOutside(false);
//			dialog.setCancelable(false);
//			progressView = (CircularProgressView) dialog
//					.findViewById(R.id.progressView);
//			progress_text = (TextView) dialog.findViewById(R.id.progresstext);
//			progress_text.setText("Searching products..");
//			progress_text.setTypeface(type);
//			com.Material.Utilities.startAnimationThreadStuff(500, progressView);
//			progressView.setIndeterminate(true);
//			dialog.show();
			latestproducts = "http://app.liquamart.com/api/products/";
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg) {
			// do above Server call here
			result = getJsonData(latestproducts);
			Log.i("result", result);
			try {
				JSONArray products=new JSONArray(result);
				names=new String[products.length()];
				descriptions=new String[products.length()];
				productids=new String[products.length()];
				imageurls=new String[products.length()];
				popular=new Boolean[products.length()];
				for(int i=0;i<products.length();i++)
				{
					JSONObject o=products.getJSONObject(i);
					productids[i]=o.getString("id");
					names[i]=o.getString("name");
					popular[i]=o.getBoolean("featured");
//					descriptions[i]=o.getString("description");
					JSONArray images=o.getJSONArray("imgUrls");
					imageurls[i]=images.getString(0);
					
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// process message
			// Toast.makeText(getApplicationContext(), result,
			// Toast.LENGTH_LONG).show();
//			dialog.dismiss();
			Log.i("result", result);
			rowItems = new ArrayList<MyProductSetter>();
	        for (int i = 0; i < names.length; i++) {            //google error
	        	if(popular[i]==true){
	            MyProductSetter item = new MyProductSetter(productids[i],names[i], imageurls[i], descriptions[i]);
	            rowItems.add(item);
	        	}
	        }
	        ProductAdapter adapter = new ProductAdapter(activity.getApplicationContext(), rowItems);
	        listView.setAdapter(adapter);
		
		}
	}
    
    

    public String getJsonData(String url) {

		StringBuilder builder = new StringBuilder();

		HttpClient client = new DefaultHttpClient();

		HttpGet httpGet = new HttpGet(url);

		try {

			HttpResponse response = client.execute(httpGet);

			StatusLine statusLine = response.getStatusLine();

			int statusCode = statusLine.getStatusCode();

			if (statusCode == 200) {

				HttpEntity entity = response.getEntity();

				InputStream content = entity.getContent();

				BufferedReader reader = new BufferedReader(

				new InputStreamReader(content));

				String line;

				while ((line = reader.readLine()) != null) {

					builder.append(line);

				}

			} else {

				// System.out.print("condition not true");

				// Log.e(ParseJSON.class.toString(),
				// "Failed to download file");

			}

		} catch (ClientProtocolException e) {

			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		}
		return builder.toString();
    }
}