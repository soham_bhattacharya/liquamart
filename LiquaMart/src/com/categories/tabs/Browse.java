package com.categories.tabs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.Material.CircularProgressView;
import com.Material.ConnectionDetector;
import com.Material.PullRefreshLayout;
import com.liquamart.R;

public class Browse extends Fragment {
	private FragmentActivity activity;
	GridView gv;
	public String[] categoriesnames;
	public String[] ids;
	public String[] descrps;
	public String[] images;
	String st="", url = "http://app.liquamart.com/api/categories";
	Typeface type;
	SwipeRefreshLayout mSwipeRefreshLayout;
	Boolean isInternetPresent = false;
	ConnectionDetector cd;
	PullRefreshLayout layout;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		activity = getActivity();
		setRetainInstance(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.browse_fragment, container,
				false);
		cd = new ConnectionDetector(activity);
		type = Typeface.createFromAsset(activity.getAssets(),
				"fonts/sanfracisco_regular.ttf");
		gv = (GridView) rootView.findViewById(R.id.gridView1);
//		mSwipeRefreshLayout=(SwipeRefreshLayout)rootView.findViewById(R.id.swipeRefreshLayout);
//		mSwipeRefreshLayout.setColorSchemeResources(R.color.blue,R.color.orange,R.color.green);
//		mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {  
//		    @Override
//		    public void onRefresh() {
//		        // Refresh items
//		    	isInternetPresent = cd.isConnectingToInternet();
//		        if (isInternetPresent) {
//		        new AsyncRefresh().execute();
//		        mSwipeRefreshLayout.setRefreshing(false);
//		        }
//		        else
//		        {
//		        	showAlertDialog(activity, "No Internet Connection",
//		                    "You don't have internet connection.", false);
//		        }
//		    }
//		});
		isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
		new Async().execute();
        }
        else
        {
        	showAlertDialog(activity, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
		
		  layout = (PullRefreshLayout)rootView.findViewById(R.id.swipeRefreshLayout);
		  layout.setRefreshStyle(PullRefreshLayout.STYLE_WATER_DROP);
		// listen refresh event
		layout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
		    @Override
		    public void onRefresh() {
		        // start refresh
		    	isInternetPresent = cd.isConnectingToInternet();
		        if (isInternetPresent) {
		        new AsyncRefresh().execute();
		        layout.setRefreshing(false);
		        }
		        else
		        {
		        	showAlertDialog(activity, "No Internet Connection",
		                    "You don't have internet connection.", false);
		        	layout.setRefreshing(false);
		        }
		        
		    }
		});

		// refresh complete 
		

		return rootView;
	}
	
	public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
 
        // Setting Dialog Title
        alertDialog.setTitle(title);
 
        // Setting Dialog Message
        alertDialog.setMessage(message);
         
        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.app_icon : R.drawable.app_icon);
 
        // Showing Alert Message
        alertDialog.show();
    }

	private class Async extends AsyncTask<String, String, String> {
		Dialog dialog;
		CircularProgressView progressView;
		TextView progress_text;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// custom dialog
			dialog = new Dialog(activity);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.custom_dialog);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			progressView = (CircularProgressView) dialog
					.findViewById(R.id.progressView);
			progress_text = (TextView) dialog.findViewById(R.id.progresstext);
			progress_text.setText("Loading..");
			progress_text.setTypeface(type);
			com.Material.Utilities.startAnimationThreadStuff(500, progressView);
			progressView.setIndeterminate(true);
			dialog.show();
			
			super.onPreExecute();
		}
		

		@Override
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub

			st = getJsonData(url);
			try {
				JSONArray result = new JSONArray(st);
				categoriesnames = new String[result.length()];
				ids=new String[result.length()];
				descrps=new String[result.length()];
				images=new String[result.length()];
				for (int i = 0; i < result.length(); i++) {
					JSONObject o = result.getJSONObject(i);
					categoriesnames[i] = o.getString("name");
					descrps[i] = o.getString("description");
					ids[i] = o.getString("id");
					images[i] = o.getString("imgUrl");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return st;
		}

		@Override
		protected void onPostExecute(String result) {
			Log.i("result",st);
			if(st.length()!=0)
			gv.setAdapter(new CustomAdapter(activity, ids,categoriesnames,descrps,
					images));
			else
				Toast.makeText(getActivity(), "Categories not found!", Toast.LENGTH_LONG).show();
			dialog.dismiss();
		}
	}
	
	private class AsyncRefresh extends AsyncTask<String, String, String> {
		

		@Override
		protected String doInBackground(String... arg0) {
			// TODO Auto-generated method stub

			st = getJsonData(url);
			try {
				JSONArray result = new JSONArray(st);
				categoriesnames = new String[result.length()];
				ids=new String[result.length()];
				descrps=new String[result.length()];
				images=new String[result.length()];
				for (int i = 0; i < result.length(); i++) {
					JSONObject o = result.getJSONObject(i);
					categoriesnames[i] = o.getString("name");
					ids[i] = o.getString("id");
					images[i] = o.getString("imgUrl");
					descrps[i] = o.getString("description");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return st;
		}

		@Override
		protected void onPostExecute(String result) {
			gv.setAdapter(new CustomAdapter(activity, ids,categoriesnames,descrps,
					images));
		}
	}

	public String getJsonData(String url) {

		StringBuilder builder = new StringBuilder();

		HttpClient client = new DefaultHttpClient();

		HttpGet httpGet = new HttpGet(url);

		try {

			HttpResponse response = client.execute(httpGet);

			StatusLine statusLine = response.getStatusLine();

			int statusCode = statusLine.getStatusCode();

			if (statusCode == 200) {

				HttpEntity entity = response.getEntity();

				InputStream content = entity.getContent();

				BufferedReader reader = new BufferedReader(

				new InputStreamReader(content));

				String line;

				while ((line = reader.readLine()) != null) {

					builder.append(line);

				}

			} else {

				// System.out.print("condition not true");

				// Log.e(ParseJSON.class.toString(),
				// "Failed to download file");

			}

		} catch (ClientProtocolException e) {

			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		}

		return builder.toString();

	}
}