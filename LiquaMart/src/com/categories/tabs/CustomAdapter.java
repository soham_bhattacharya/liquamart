package com.categories.tabs;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.liquamart.R;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class CustomAdapter extends BaseAdapter {

	String[] result;
	String[] ids;
	String[] descrps;
	FragmentActivity context;
	String[] imageId;
	Typeface type;
	private static LayoutInflater inflater = null;

	public CustomAdapter(FragmentActivity activity,String[] idss, String[] prgmNameList,String[] descrpss,
			String[] prgmImages) {
		// TODO Auto-generated constructor stub
		ids=idss;
		result = prgmNameList;
		context = activity;
		imageId = prgmImages;
		descrps=descrpss;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return result.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	public class Holder {
		TextView tv;
		TextView tv2;
		ImageView img;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder = new Holder();
		View rowView;

		type = Typeface.createFromAsset(context.getAssets(),
				"fonts/sanfracisco_regular.ttf");
		convertView = inflater.inflate(R.layout.custom_list_item, null);
		holder.tv = (TextView) convertView.findViewById(R.id.product_name);
		holder.tv2 = (TextView) convertView.findViewById(R.id.product_descrp);
		holder.img = (ImageView) convertView.findViewById(R.id.imageView1);

		holder.tv.setText(result[position]);
		holder.tv2.setText(descrps[position]);
		holder.tv.setTypeface(type);
		holder.tv2.setTypeface(type);
		try {
			DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
					.cacheOnDisc(true).cacheInMemory(true)
					.imageScaleType(ImageScaleType.EXACTLY)
					.displayer(new FadeInBitmapDisplayer(300)).build();

			ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
					context).defaultDisplayImageOptions(defaultOptions)
					.memoryCache(new WeakMemoryCache())
					.discCacheSize(100 * 1024 * 1024).build();

			ImageLoader.getInstance().init(config);
			ImageLoader imageLoader = ImageLoader.getInstance();
			// DisplayImageOptions options = new
			// DisplayImageOptions.Builder()
			// .cacheInMemory(true).cacheOnDisc(true)
			// .resetViewBeforeLoading(true)
			// .showImageForEmptyUri(R.drawable.user_image_default)
			// .showImageOnFail(R.drawable.user_image_default)
			// .showImageOnLoading(R.drawable.user_image_default).build();

			holder.img.setScaleType(ScaleType.CENTER_CROP);
			DisplayMetrics metrics = new DisplayMetrics();
			context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
			switch(metrics.densityDpi){
			     case DisplayMetrics.DENSITY_LOW:
			                holder.img.setLayoutParams(new RelativeLayout.LayoutParams(metrics.DENSITY_LOW, metrics.DENSITY_LOW));
			                break;
			     case DisplayMetrics.DENSITY_MEDIUM:
			    	 holder.img.setLayoutParams(new RelativeLayout.LayoutParams(metrics.DENSITY_MEDIUM, metrics.DENSITY_MEDIUM));
			                break;
			     case DisplayMetrics.DENSITY_HIGH:
			    	 holder.img.setLayoutParams(new RelativeLayout.LayoutParams(metrics.DENSITY_HIGH, metrics.DENSITY_HIGH));
			                 break;
			}
			imageLoader.displayImage(imageId[position], holder.img);
			
			

		} catch (Exception e) {

		}

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try{
				Intent i=new Intent("com.liquamart.SubCategories");
				i.putExtra("id", ids[position]);
				i.putExtra("subcategory", result[position]);
				context.startActivity(i);
				}
				catch(Exception e)
				{
					Toast.makeText(context, "Something went wrong!", Toast.LENGTH_LONG).show();
				}
			}
		});

		return convertView;
	}

}
