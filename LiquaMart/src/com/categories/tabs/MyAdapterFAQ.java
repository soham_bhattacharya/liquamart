package com.categories.tabs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.Material.CircleImageView;
import com.Material.CircularProgressView;
import com.Material.ConnectionDetector;
import com.liquamart.Categories;
import com.liquamart.Contact;
import com.liquamart.FAQs;
import com.liquamart.Login;
import com.liquamart.MyProducts;
import com.liquamart.Question;
import com.liquamart.R;
import com.liquamart.SubCategories;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

/**
 * Created by hp1 on 28-12-2014.
 */
public class MyAdapterFAQ extends RecyclerView.Adapter<MyAdapterFAQ.ViewHolder> {

	private static final int TYPE_HEADER = 0; // Declaring Variable to
												// Understand which View is
												// being worked on
												// IF the view under inflation
												// and population is header or
												// Item
	private static final int TYPE_ITEM = 1;

	private String mNavTitles[]; // String Array to store the passed titles
									// Value from MainActivity.java
	private int mIcons[]; // Int Array to store the passed icons resource value
							// from MainActivity.java

	private String name; // String Resource for header View Name
	private String profile; // int Resource for header view profile picture
	private String email; // String Resource for header view email
	Context con;
	Activity activity;
	Typeface type;
	String myproductsurl;
	Boolean isInternetPresent = false;
	ConnectionDetector cd;

	// Creating a ViewHolder which extends the RecyclerView View Holder
	// ViewHolder are used to to store the inflated views in order to recycle
	// them

	public static class ViewHolder extends RecyclerView.ViewHolder {
		int Holderid;

		TextView textView;
		ImageView imageView;
		CircleImageView profile_pic;
		TextView Name;
		TextView email;

		public ViewHolder(View itemView, int ViewType) { // Creating ViewHolder
															// Constructor with
															// View and viewType
															// As a parameter
			super(itemView);

			// Here we set the appropriate view in accordance with the the view
			// type as passed when the holder object is created

			if (ViewType == TYPE_ITEM) {
				textView = (TextView) itemView.findViewById(R.id.rowText); // Creating
																			// TextView
																			// object
																			// with
																			// the
																			// id
																			// of
																			// textView
																			// from
																			// item_row.xml
				imageView = (ImageView) itemView.findViewById(R.id.rowIcon);// Creating
																			// ImageView
																			// object
																			// with
																			// the
																			// id
																			// of
																			// ImageView
																			// from
																			// item_row.xml
				Holderid = 1; // setting holder id as 1 as the object being
								// populated are of type item row
			} else {

				Name = (TextView) itemView.findViewById(R.id.name); // Creating
																	// Text View
																	// object
																	// from
																	// header.xml
																	// for name
				email = (TextView) itemView.findViewById(R.id.email); // Creating
																		// Text
																		// View
																		// object
																		// from
																		// header.xml
																		// for
																		// email
				profile_pic = (CircleImageView) itemView
						.findViewById(R.id.circleView);// Creating Image view
														// object from
														// header.xml for
														// profile pic
				Holderid = 0; // Setting holder id = 0 as the object being
								// populated are of type header view
			}
		}

	}

	public MyAdapterFAQ(Context context, Activity activity1, String Titles[],
			int Icons[], String Name, String Email, String photo) { // MyAdapter
																	// Constructor
																	// with
																	// titles
																	// and icons
																	// parameter
		// titles, icons, name, email, profile pic are passed from the main
		// activity as we
		mNavTitles = Titles; // have seen earlier
		mIcons = Icons;
		name = Name;
		email = Email;
		profile = photo;
		con = context;// here we assign those passed values to the values we
						// declared here
		// in adapter
		activity = activity1;

	}

	// Below first we ovverride the method onCreateViewHolder which is called
	// when the ViewHolder is
	// Created, In this method we inflate the item_row.xml layout if the
	// viewType is Type_ITEM or else we inflate header.xml
	// if the viewType is TYPE_HEADER
	// and pass it to the view holder

	@Override
	public MyAdapterFAQ.ViewHolder onCreateViewHolder(ViewGroup parent,
			int viewType) {

		if (viewType == TYPE_ITEM) {
			View v = LayoutInflater.from(parent.getContext()).inflate(
					R.layout.item_row, parent, false); // Inflating the layout

			ViewHolder vhItem = new ViewHolder(v, viewType); // Creating
																// ViewHolder
																// and passing
																// the object of
																// type view

			return vhItem; // Returning the created object

			// inflate your layout and pass it to view holder

		} else if (viewType == TYPE_HEADER) {

			View v = LayoutInflater.from(parent.getContext()).inflate(
					R.layout.header, parent, false); // Inflating the layout

			ViewHolder vhHeader = new ViewHolder(v, viewType); // Creating
																// ViewHolder
																// and passing
																// the object of
																// type view

			return vhHeader; // returning the object created

		}
		return null;

	}

	// Next we override a method which is called when the item in a row is
	// needed to be displayed, here the int position
	// Tells us item at which position is being constructed to be displayed and
	// the holder id of the holder object tell us
	// which view type is being created 1 for item row
	@Override
	public void onBindViewHolder(final MyAdapterFAQ.ViewHolder holder,
			final int position) {
		type = Typeface.createFromAsset(con.getAssets(),
				"fonts/sanfracisco_regular.ttf");
		if (holder.Holderid == 1) { // as the list view is going to be called
									// after the header view so we decrement the
									// position by 1 and pass it to the holder
									// while setting the text and image
			holder.textView.setText(mNavTitles[position - 1]); // Setting the
																// Text with the
																// array of our
																// Titles
			holder.textView.setTypeface(type);
			holder.imageView.setImageResource(mIcons[position - 1]);// Settimg
																	// the image
																	// with
																	// array of
																	// our icons
			cd = new ConnectionDetector(activity);
			holder.textView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(holder.textView.getText().equals("Home"))
					{
						FAQs.Drawer.closeDrawers();
						new Handler().postDelayed(new Runnable() {
					        @Override
					        public void run() {
					        	activity.finish();
					        }
				    }, 200);
					}
						
				else if (holder.textView.getText().equals("Logout")) {
					isInternetPresent = cd.isConnectingToInternet();
	                if (isInternetPresent) {
						new LogoutfromServer().execute();
						FAQs.Drawer.closeDrawers();
	                }
	                else
	                {
	                	showAlertDialog(activity, "No Internet Connection",
	                            "You don't have internet connection.", false);
	                }
					} else if (holder.textView.getText().equals("My Products")) {
						FAQs.Drawer.closeDrawers();
						new Handler().postDelayed(new Runnable() {
					        @Override
					        public void run() {
							Intent i = new Intent("com.liquamart.MyProducts");
							i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							con.startActivity(i);
							activity.finish();
					        }
					    }, 200);
					} else if (holder.textView.getText().equals(
							"Contact Support")) {
						FAQs.Drawer.closeDrawers();
						new Handler().postDelayed(new Runnable() {
					        @Override
					        public void run() {
							Intent i = new Intent("com.liquamart.Contact");
							i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							con.startActivity(i);
							activity.finish();
					        }
					    }, 200);
					}
					else if(holder.textView.getText().equals("FAQ's"))
					{
						FAQs.Drawer.closeDrawers();
					}
					else if(holder.textView.getText().equals("Order Replacement"))
					{
						FAQs.Drawer.closeDrawers();
						new Handler().postDelayed(new Runnable() {
					        @Override
					        public void run() {
							Intent i = new Intent("com.liquamart.Replacement");
							i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							con.startActivity(i);
							activity.finish();
					}
						 }, 200);
					}
					else if(holder.textView.getText().equals("Favorites"))
					{
						FAQs.Drawer.closeDrawers();
						new Handler().postDelayed(new Runnable() {
					        @Override
					        public void run() {
							Intent i = new Intent("com.liquamart.Favourites");
							i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							con.startActivity(i);
							activity.finish();
					}
						 }, 200);
					}
					else if(holder.textView.getText().equals("Profile Settings"))
					{
						FAQs.Drawer.closeDrawers();
						new Handler().postDelayed(new Runnable() {
					        @Override
					        public void run() {
							Intent i = new Intent("com.liquamart.ProfileSettings");
							i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							con.startActivity(i);
							activity.finish();
					}
						 }, 200);
					}
//					Categories.Drawer.closeDrawers();
				}
			});
			holder.imageView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					int pos = position - 1;
					if(pos==0){
						FAQs.Drawer.closeDrawers();
						new Handler().postDelayed(new Runnable() {
					        @Override
					        public void run() {
					        	activity.finish();
					        }
				    }, 200);
					}
					else if (pos == 7) {
						isInternetPresent = cd.isConnectingToInternet();
		                if (isInternetPresent) {
							new LogoutfromServer().execute();
							FAQs.Drawer.closeDrawers();
		                }
		                else
		                {
		                	showAlertDialog(activity, "No Internet Connection",
		                            "You don't have internet connection.", false);
		                }
					} else if (pos == 1) {
						FAQs.Drawer.closeDrawers();
						new Handler().postDelayed(new Runnable() {
					        @Override
					        public void run() {
							Intent i = new Intent("com.liquamart.MyProducts");
							i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							con.startActivity(i);
							activity.finish();
					        }
					    }, 200);
						}
					 else if (pos == 5) {
						 FAQs.Drawer.closeDrawers();
						 new Handler().postDelayed(new Runnable() {
						        @Override
						        public void run() {
							Intent i = new Intent("com.liquamart.Contact");
							i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							con.startActivity(i);
							activity.finish();
						        }
						    }, 200);
					}
					 else if(pos==4)
					 {
						 FAQs.Drawer.closeDrawers();
					 }
					
					 else if(pos==3)
					 {
						 FAQs.Drawer.closeDrawers();
							new Handler().postDelayed(new Runnable() {
						        @Override
						        public void run() {
								Intent i = new Intent("com.liquamart.Replacement");
								i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								con.startActivity(i);
								activity.finish();
						}
							 }, 200);
						
					 }
					 else if(pos==2)
					 {
						 FAQs.Drawer.closeDrawers();
							new Handler().postDelayed(new Runnable() {
						        @Override
						        public void run() {
								Intent i = new Intent("com.liquamart.Favourites");
								i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								con.startActivity(i);
								activity.finish();
						}
							 }, 200);
					 }
					 else if(pos==6)
					 {
						 FAQs.Drawer.closeDrawers();
							new Handler().postDelayed(new Runnable() {
						        @Override
						        public void run() {
								Intent i = new Intent("com.liquamart.ProfileSettings");
								i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								con.startActivity(i);
								activity.finish();
						}
							 }, 200);
					 }
				}
			});

		} else {

			holder.Name.setText(name);
			holder.Name.setTypeface(type);
			holder.email.setText(email);
			holder.email.setTypeface(type);
			try {
				DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
						.cacheOnDisc(true).cacheInMemory(true)
						.imageScaleType(ImageScaleType.EXACTLY)
						.displayer(new FadeInBitmapDisplayer(300)).build();

				ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
						con).defaultDisplayImageOptions(defaultOptions)
						.memoryCache(new WeakMemoryCache())
						.discCacheSize(100 * 1024 * 1024).build();

				ImageLoader.getInstance().init(config);
				ImageLoader imageLoader = ImageLoader.getInstance();
				// DisplayImageOptions options = new
				// DisplayImageOptions.Builder()
				// .cacheInMemory(true).cacheOnDisc(true)
				// .resetViewBeforeLoading(true)
				// .showImageForEmptyUri(R.drawable.user_image_default)
				// .showImageOnFail(R.drawable.user_image_default)
				// .showImageOnLoading(R.drawable.user_image_default).build();

				imageLoader.displayImage(profile, holder.profile_pic);

			} catch (Exception e) {

			}
		}
	}

	// This method returns the number of items present in the list
	@Override
	public int getItemCount() {
		return mNavTitles.length + 1; // the number of items in the list will be
										// +1 the titles including the header
										// view.
	}
	public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
 
        // Setting Dialog Title
        alertDialog.setTitle(title);
 
        // Setting Dialog Message
        alertDialog.setMessage(message);
         
        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.app_icon : R.drawable.app_icon);
 
        // Showing Alert Message
        alertDialog.show();
    }

	// Witht the following method we check what type of view is being passed
	@Override
	public int getItemViewType(int position) {
		if (isPositionHeader(position))
			return TYPE_HEADER;

		return TYPE_ITEM;
	}

	private boolean isPositionHeader(int position) {
		return position == 0;
	}

	private class LogoutfromServer extends AsyncTask<String, String, String> {
		Dialog dialog;
		String accessToken;
		CircularProgressView progressView;
		TextView progress_text;
		String result, url;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// custom dialog
			dialog = new Dialog(activity);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.custom_dialog);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			progressView = (CircularProgressView) dialog
					.findViewById(R.id.progressView);
			progress_text = (TextView) dialog.findViewById(R.id.progresstext);
			progress_text.setText("Signing Out..");
			progress_text.setTypeface(type);
			com.Material.Utilities.startAnimationThreadStuff(500, progressView);
			progressView.setIndeterminate(true);
			dialog.show();
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(activity);
			accessToken = preferences.getString("accessToken", "");
			url = "http://app.liquamart.com/api/users/logout?access_token="
					+ accessToken;
			Log.i("logoutapi", url);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg) {
			// do above Server call here
			result = getJsonData(url);
			Log.i("result", result);
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// process message
			// Toast.makeText(getApplicationContext(), result,
			// Toast.LENGTH_LONG).show();
			dialog.dismiss();
			Log.i("result", result);
			FAQs.signOutFromGplus();
			FAQs.callFacebookLogout(con);
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(con);
			SharedPreferences.Editor editor = preferences.edit();
			editor.putString("name", "");
			editor.putString("email", "");
			editor.putString("progress", "login");
			editor.putString("logintype", "");
			editor.putString("photourl", "");
			editor.putString("accessToken", "");
			editor.putString("userid", "");
			editor.putString("adapterprogress", "");
			editor.putString("state","" );
			editor.putString("phone","" );
			editor.apply();
			Intent i = new Intent(con, Login.class);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TASK);
			con.startActivity(i);
			activity.finish();

		}
	}

	

	public String getJsonData(String url) {

		StringBuilder builder = new StringBuilder();

		HttpClient client = new DefaultHttpClient();

		HttpPost httpGet = new HttpPost(url);

		try {

			HttpResponse response = client.execute(httpGet);

			StatusLine statusLine = response.getStatusLine();

			int statusCode = statusLine.getStatusCode();

			if (statusCode == 200) {

				HttpEntity entity = response.getEntity();

				InputStream content = entity.getContent();

				BufferedReader reader = new BufferedReader(

				new InputStreamReader(content));

				String line;

				while ((line = reader.readLine()) != null) {

					builder.append(line);

				}

			} else {

				// System.out.print("condition not true");

				// Log.e(ParseJSON.class.toString(),
				// "Failed to download file");

			}

		} catch (ClientProtocolException e) {

			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		}

		return builder.toString();

	}

	

}