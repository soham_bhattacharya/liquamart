package com.products;

public class MyProductSetter {
	private String name, ImageURL, date,ProductID;

	public MyProductSetter() {
	}

	public MyProductSetter(String productid,String name, String imageurl, String date) {
		this.ProductID=productid;
		this.name = name;
		this.ImageURL = imageurl;
		this.date = date;

	}
	
	public String getProductID() {
		return ProductID;
	}

	public void setProductID(String ProductID) {
		this.ProductID = ProductID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getImageURL() {
		return ImageURL;
	}

	public void setImageURL(String ImageURL) {
		this.ImageURL = ImageURL;
	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	
	
}
