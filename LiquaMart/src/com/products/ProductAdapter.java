package com.products;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.Material.CircleImageView;
import com.liquamart.R;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class ProductAdapter extends BaseAdapter {
	Context context;
    List<MyProductSetter> rowItems;
	
	public ProductAdapter(Context context, List<MyProductSetter> items) {
        this.context = context;
        this.rowItems = items;
    }

	/*private view holder class*/
    private class ViewHolder {
    	CircleImageView imageView;
        TextView name;
        TextView date;
    }
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		 return rowItems.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return rowItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return rowItems.indexOf(getItem(position));
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder = null;
        
        LayoutInflater mInflater = (LayoutInflater) 
            context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.product_item, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.product_name);
            holder.date = (TextView) convertView.findViewById(R.id.product_date);
            holder.imageView = (CircleImageView) convertView.findViewById(R.id.icon);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
         
        final MyProductSetter rowItem = (MyProductSetter) getItem(position);
         
        holder.name.setText(rowItem.getName());
        holder.date.setText(rowItem.getDate());
        try {
			DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
					.cacheOnDisc(true).cacheInMemory(true)
					.imageScaleType(ImageScaleType.EXACTLY)
					.displayer(new FadeInBitmapDisplayer(300)).build();

			ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
					context).defaultDisplayImageOptions(defaultOptions)
					.memoryCache(new WeakMemoryCache())
					.discCacheSize(100 * 1024 * 1024).build();

			ImageLoader.getInstance().init(config);
			ImageLoader imageLoader = ImageLoader.getInstance();
			// DisplayImageOptions options = new
			// DisplayImageOptions.Builder()
			// .cacheInMemory(true).cacheOnDisc(true)
			// .resetViewBeforeLoading(true)
			// .showImageForEmptyUri(R.drawable.user_image_default)
			// .showImageOnFail(R.drawable.user_image_default)
			// .showImageOnLoading(R.drawable.user_image_default).build();

			imageLoader.displayImage(rowItem.getImageURL(), holder.imageView);

		} catch (Exception e) {

		}
//        holder.descrp.setText(rowItem.getDescrp());
//        holder.date.setText(rowItem.getDate());
        convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent("com.liquamart.ProductDetails");
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				i.putExtra("id", rowItem.getProductID());
				context.startActivity(i);
			}
		});
         
        return convertView;
	}

}
