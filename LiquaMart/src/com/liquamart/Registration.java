package com.liquamart;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.Material.CircleImageView;
import com.Material.CircularProgressView;
import com.Material.ConnectionDetector;
import com.Material.MaterialEditText;
import com.Material.MaterialSpinner;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
//import com.google.common.collect.ImmutableMap;
//import com.strongloop.android.loopback.ModelRepository;
//import com.strongloop.android.loopback.RestAdapter;
//import com.strongloop.android.loopback.callbacks.VoidCallback;

@SuppressWarnings("deprecation")
public class Registration extends ActionBarActivity implements
		ConnectionCallbacks, OnConnectionFailedListener {
	android.support.v7.app.ActionBar ab;
	Button yes, no;
	private GoogleApiClient mGoogleApiClient;
	private UiLifecycleHelper uiHelper;
	private static final int RC_SIGN_IN = 0;
	private boolean mSignInClicked;
	private ConnectionResult mConnectionResult;
	// private SignInButton btnSignIn;
	private boolean mIntentInProgress;
	MaterialSpinner spinner;
	MaterialEditText name, email, password, phone;
	TextView hiuser, text_msg;
	Button createaccount, skip;
	String name1 = "", email1 = "", logintype = "", nameperson = "",
			emailperson = "", result = "";
	String[] states;
	String selected_state,PHOTO;
	CircleImageView profile_pic;
	CircularProgressView progressView;
	Dialog dialog;
	TextView progress_text;
	boolean happenedonce = false;
	Typeface type;
	Boolean isInternetPresent = false;
	ConnectionDetector cd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.registration_fragment);
		ab = getSupportActionBar();
		ab.hide();
		cd = new ConnectionDetector(getApplicationContext());
		Bundle extras = getIntent().getExtras();
		logintype = extras.getString("login");
		emailperson = extras.getString("email");

		String[] ITEMS = getResources().getStringArray(R.array.states);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, ITEMS);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner = (MaterialSpinner) findViewById(R.id.spinner_states);

		spinner.setAdapter(adapter);
		name = (MaterialEditText) findViewById(R.id.text_name);
		email = (MaterialEditText) findViewById(R.id.text_email);
		password = (MaterialEditText) findViewById(R.id.text_password);
		hiuser = (TextView) findViewById(R.id.txt_hi_user);
		phone = (MaterialEditText) findViewById(R.id.text_phone);
		text_msg = (TextView) findViewById(R.id.txt_register_msg);
		createaccount = (Button) findViewById(R.id.create_account);
		profile_pic = (CircleImageView) findViewById(R.id.icon);
		skip = (Button) findViewById(R.id.skip);
		type = Typeface.createFromAsset(getAssets(),
				"fonts/sanfracisco_regular.ttf");
		name.setTypeface(type);
		email.setTypeface(type);
		phone.setTypeface(type);
		hiuser.setTypeface(type);
		password.setTypeface(type);
		text_msg.setTypeface(type);
		createaccount.setTypeface(type);
		skip.setTypeface(type);

		if (logintype.equals("facebook") || logintype.equals("google")) {
			password.setVisibility(View.GONE);
		} else
			hiuser.setVisibility(View.GONE);
		if (logintype.equals("facebook")) {
			dialog = new Dialog(Registration.this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.custom_dialog);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);

			progressView = (CircularProgressView) dialog
					.findViewById(R.id.progressView);
			progress_text = (TextView) dialog.findViewById(R.id.progresstext);
			progress_text.setText("Getting information..");
			progress_text.setTypeface(type);
			com.Material.Utilities.startAnimationThreadStuff(500, progressView);
			progressView.setIndeterminate(true);
			dialog.show();
			getinfo();
		} else if (logintype.equals("google")) {
			dialog = new Dialog(Registration.this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.custom_dialog);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);

			progressView = (CircularProgressView) dialog
					.findViewById(R.id.progressView);
			progress_text = (TextView) dialog.findViewById(R.id.progresstext);
			progress_text.setText("Getting information..");
			progress_text.setTypeface(type);
			com.Material.Utilities.startAnimationThreadStuff(500, progressView);
			progressView.setIndeterminate(true);
			dialog.show();
		}

		createaccount.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// new NodeResponseEmail().execute();
				SharedPreferences preferences = PreferenceManager
						.getDefaultSharedPreferences(Registration.this);
				SharedPreferences.Editor editor = preferences.edit();
				
				if (logintype.equals("facebook") || logintype.equals("google")) {
					if (name.getText().length() == 0)
						Toast.makeText(getApplicationContext(),
								"Please enter Name", Toast.LENGTH_LONG).show();
					else if (email.getText().length() == 0)
						Toast.makeText(getApplicationContext(),
								"Please enter Email Address", Toast.LENGTH_LONG)
								.show();
					else if (!isValidEmailAddress(email.getText().toString()))
						Toast.makeText(getApplicationContext(),
								"Please enter Correct Email Address", Toast.LENGTH_LONG)
								.show();
					else if (phone.getText().length() == 0)
						Toast.makeText(getApplicationContext(),
								"Please enter Phone Number", Toast.LENGTH_LONG)
								.show();
					else if (spinner.getSelectedItem().toString()
							.equals("State"))
						Toast.makeText(getApplicationContext(),
								"Please select a state", Toast.LENGTH_LONG)
								.show();
					else {
						isInternetPresent = cd.isConnectingToInternet();
		                if (isInternetPresent) {
		                	editor.putString("progress", "createaccount");
							editor.apply();
						new UserUpdate().execute();
						
		                }
		                else
		                {
		                	showAlertDialog(Registration.this, "No Internet Connection",
		                            "You don't have internet connection.", false);
		                }
						
					}
				} else {
					if (name.getText().length() == 0)
						Toast.makeText(getApplicationContext(),
								"Please enter Name", Toast.LENGTH_LONG).show();
					else if (password.getText().length() == 0)
						Toast.makeText(getApplicationContext(),
								"Please enter Password", Toast.LENGTH_LONG)
								.show();
					else if (password.getText().length() <= 5)
						Toast.makeText(getApplicationContext(),
								"Your password must be at least 6 characters long. Please try another.", Toast.LENGTH_LONG)
								.show();
					else if (email.getText().length() == 0)
						Toast.makeText(getApplicationContext(),
								"Please enter Email Address", Toast.LENGTH_LONG)
								.show();
					else if (!isValidEmailAddress(email.getText().toString()))
						Toast.makeText(getApplicationContext(),
								"Please enter Correct Email Address", Toast.LENGTH_LONG)
								.show();
					else if (phone.getText().length() == 0)
						Toast.makeText(getApplicationContext(),
								"Please enter Phone Number", Toast.LENGTH_LONG)
								.show();
					else if (spinner.getSelectedItem().toString()
							.equals("State"))
						Toast.makeText(getApplicationContext(),
								"Please select a state", Toast.LENGTH_LONG)
								.show();
					else {
						isInternetPresent = cd.isConnectingToInternet();
		                if (isInternetPresent) {
						new NodeResponseEmail().execute();
		                }
		                else
		                {
		                	showAlertDialog(Registration.this, "No Internet Connection",
		                            "You don't have internet connection.", false);
		                }
						
					}
				}
			}
		});

		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				selected_state = spinner.getSelectedItem().toString();

			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		skip.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startActivity(new Intent("com.liquamart.Question"));
				finish();
			}
		});

		// Loopback implementation

		/*
		 * RestAdapter adapter = new
		 * RestAdapter(getApplicationContext(),"http://myserver:3000/api");
		 * WidgetRepository repository =
		 * adapter.createRepository(WidgetRepository.class); Widget pencil =
		 * repository.createObject(ImmutableMap.of("name", "Pencil"));
		 * pencil.price = new BigDecimal("1.50"); pencil.save(new VoidCallback()
		 * {
		 * 
		 * @Override public void onSuccess() { // Pencil now exists on the
		 * server! }
		 * 
		 * @Override public void onError(Throwable t) { // save failed, handle
		 * the error } });
		 */
		// yes = (Button) findViewById(R.id.yesiown);
		// no = (Button) findViewById(R.id.noidont);
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(Plus.API, Plus.PlusOptions.builder().build())
				.addScope(Plus.SCOPE_PLUS_LOGIN).build();
		// yes.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View arg0) {
		// // TODO Auto-generated method stub
		// callFacebookLogout(getApplicationContext());
		// signOutFromGplus();
		// finish();
		//
		// }
		// });

	}

	/*
	 * public class WidgetRepository extends ModelRepository<Widget> { public
	 * WidgetRepository() { super("widget", Widget.class); } }
	 */

	// to create account
	
	public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
 
        // Setting Dialog Title
        alertDialog.setTitle(title);
 
        // Setting Dialog Message
        alertDialog.setMessage(message);
         
        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.app_icon : R.drawable.app_icon);
 
        // Showing Alert Message
        alertDialog.show();
    }
	
	public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
 }
	
	private class NodeResponseEmail extends AsyncTask<String, String, String> {

		Dialog dialog;
		CircularProgressView progressView;
		TextView progress_text;
		String accessToken, userid;
		int errorcode;
		Boolean proceed=true;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// custom dialog
			dialog = new Dialog(Registration.this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.custom_dialog);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			progressView = (CircularProgressView) dialog
					.findViewById(R.id.progressView);
			progress_text = (TextView) dialog.findViewById(R.id.progresstext);
			progress_text.setText("Creating Account..");
			progress_text.setTypeface(type);
			com.Material.Utilities.startAnimationThreadStuff(500, progressView);
			progressView.setIndeterminate(true);
			dialog.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg) {
			// do above Server call here
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://app.liquamart.com/api/users");

			try {
				// Add your data
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						2);
				// Add all the variables
				nameValuePairs.add(new BasicNameValuePair("fname", name
						.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("email", email
						.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("contact", phone
						.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("state",
						selected_state));
				nameValuePairs.add(new BasicNameValuePair("password", password
						.getText().toString()));

				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();

				InputStream in = entity.getContent();

				BufferedReader br = new BufferedReader(new InputStreamReader(
						in, "UTF-8"), 8);

				StringBuilder sb = new StringBuilder();
				String line = null;

				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");

				}
				result = sb.toString();
				
				try {
					JSONObject data=new JSONObject(result);
					JSONObject error=data.getJSONObject("error");
					errorcode=error.getInt("status");
					proceed=false;
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					proceed=true;
					e.printStackTrace();
				}
//				try {
//					JSONObject rs = new JSONObject(result);
//					accessToken = rs.getString("accessToken");
//					userid = rs.getString("id");
//					Log.d("accessToken", accessToken);
//
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// process message
			// Toast.makeText(getApplicationContext(), result,
			// Toast.LENGTH_LONG).show();
			Log.i("result", result);
			// progressView.setVisibility(View.GONE);
			dialog.dismiss();
			Log.i("result", result);
			if(proceed==true)
			new UserLogin().execute();
			else if(proceed==false && errorcode==422)
				Toast.makeText(getApplicationContext(), "Email already exists. Please try another.", Toast.LENGTH_LONG).show();
			
			// editor.putString("photourl", personPhotoUrl+"0");
		}
	}

	private class UserUpdate extends AsyncTask<String, String, String> {

		Dialog dialog;
		CircularProgressView progressView;
		TextView progress_text;
		String accessToken, userid, updateurl;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// custom dialog
			dialog = new Dialog(Registration.this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.custom_dialog);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			progressView = (CircularProgressView) dialog
					.findViewById(R.id.progressView);
			progress_text = (TextView) dialog.findViewById(R.id.progresstext);
			progress_text.setText("Creating Account..");
			progress_text.setTypeface(type);
			com.Material.Utilities.startAnimationThreadStuff(500, progressView);
			progressView.setIndeterminate(true);
			dialog.show();
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(Registration.this);
			accessToken = preferences.getString("accessToken", "");
			userid = preferences.getString("userid", "");
			Log.i("id", userid);
			updateurl = "http://app.liquamart.com/api/users/" + userid
					+ "?access_token=" + accessToken;
			Log.i("updateurl", updateurl);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg) {
			// do above Server call here
			HttpClient httpclient = new DefaultHttpClient();
			HttpPut httppost = new HttpPut(updateurl);

			try {
				// Add your data
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						2);
				// Add all the variables
				nameValuePairs.add(new BasicNameValuePair("email", email
						.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("contact", phone
						.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("state",
						selected_state));
				nameValuePairs.add(new BasicNameValuePair("photourl",
						PHOTO));

				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();

				InputStream in = entity.getContent();

				BufferedReader br = new BufferedReader(new InputStreamReader(
						in, "UTF-8"), 8);

				StringBuilder sb = new StringBuilder();
				String line = null;

				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");

				}
				result = sb.toString();
				// try {
				// JSONObject rs = new JSONObject(result);
				// accessToken = rs.getString("accessToken");
				// userid = rs.getString("id");
				// Log.d("accessToken", accessToken);
				//
				// } catch (JSONException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// process message
			// Toast.makeText(getApplicationContext(), result,
			// Toast.LENGTH_LONG).show();
			Log.i("result", result);
			// progressView.setVisibility(View.GONE);
			dialog.dismiss();
//			Log.i("result", result);
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(Registration.this);
			SharedPreferences.Editor editor = preferences.edit();
			editor.putString("state", selected_state);
			editor.putString("phone", phone.getText().toString());
			editor.putString("progress", "main");
			editor.apply();
			Intent i = new Intent("com.liquamart.Question");
			startActivity(i);
			finish();

			// editor.putString("photourl", personPhotoUrl+"0");
		}
	}
	
	private class UserLogin extends AsyncTask<String, String, String> {

		Dialog dialog;
		CircularProgressView progressView;
		TextView progress_text;
		String accessToken, userid, loginurl,name2,contact2,state2;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// custom dialog
			dialog = new Dialog(Registration.this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.custom_dialog);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			progressView = (CircularProgressView) dialog
					.findViewById(R.id.progressView);
			progress_text = (TextView) dialog.findViewById(R.id.progresstext);
			progress_text.setText("Logging in..");
			progress_text.setTypeface(type);
			com.Material.Utilities.startAnimationThreadStuff(500, progressView);
			progressView.setIndeterminate(true);
			dialog.show();
			loginurl = "http://app.liquamart.com/api/users/login";
			Log.i("loginurl", loginurl);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg) {
			// do above Server call here
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(loginurl);

			try {
				// Add your data
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						2);
				// Add all the variables
				Log.i("result", email
						.getText().toString());
				nameValuePairs.add(new BasicNameValuePair("email", email
						.getText().toString()));
			    nameValuePairs.add(new BasicNameValuePair("password", password
						.getText().toString()));

				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();

				InputStream in = entity.getContent();

				BufferedReader br = new BufferedReader(new InputStreamReader(
						in, "UTF-8"), 8);

				StringBuilder sb = new StringBuilder();
				String line = null;

				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");

				}
				result = sb.toString();
				 try {
				 JSONObject rs = new JSONObject(result);
				 accessToken = rs.getString("id");
				 userid = rs.getString("userId");
				 name2 = rs.getString("name");
				 contact2 = rs.getString("contact");
				 state2 = rs.getString("state");
				 Log.d("accessToken", accessToken);
				
				 } catch (JSONException e) {
				 // TODO Auto-generated catch block
				 e.printStackTrace();
				 }

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// process message
			// Toast.makeText(getApplicationContext(), result,
			// Toast.LENGTH_LONG).show();
			Log.i("result", result);
			// progressView.setVisibility(View.GONE);
			dialog.dismiss();
//			Log.i("result", result);
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(Registration.this);
			SharedPreferences.Editor editor = preferences.edit();
			editor.putString("accessToken", accessToken);
			editor.putString("userid", userid);
			editor.putString("name", name.getText().toString());
			editor.putString("email", email.getText().toString());
			editor.putString("progress", "main");
			editor.putString("logintype", "liqua");
			editor.putString("state", selected_state);
			editor.putString("phone", phone.getText().toString());
			editor.apply();
			Intent i = new Intent("com.liquamart.Question");
			startActivity(i);
			finish();

			// editor.putString("photourl", personPhotoUrl+"0");
		}
	}


	@SuppressLint("DefaultLocale")
	private void getProfileInformation() {
		try {
			if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
				Person currentPerson = Plus.PeopleApi
						.getCurrentPerson(mGoogleApiClient);
				String personName = currentPerson.getDisplayName();

				String personPhotoUrl = currentPerson.getImage().getUrl();
				String personGooglePlusProfile = currentPerson.getUrl();
				String email1 = Plus.AccountApi
						.getAccountName(mGoogleApiClient);
				Log.e("Info", "Name: " + personName + ", plusProfile: "
						+ personGooglePlusProfile + ", email: " + email1
						+ ", Image: " + personPhotoUrl + "gender "
						+ currentPerson.getGender() + " location"
						+ currentPerson.getCurrentLocation() + "");
				nameperson = currentPerson.getDisplayName();
				emailperson = Plus.AccountApi.getAccountName(mGoogleApiClient);
				name.setText(nameperson);
				email.setText(emailperson);
				SharedPreferences preferences = PreferenceManager
						.getDefaultSharedPreferences(Registration.this);
				SharedPreferences.Editor editor = preferences.edit();
				editor.putString("name", nameperson);
				editor.putString("email", emailperson);
				editor.putString("progress", "registration");
				editor.putString("logintype", "google");
				editor.putString("photourl", personPhotoUrl + "0");
				PHOTO=personPhotoUrl+"0";
				editor.apply();
				String firstname = nameperson.substring(0,
						nameperson.indexOf(' '));
				firstname = firstname.substring(0, 1).toUpperCase()
						+ firstname.substring(1);
				hiuser.setText("Hi " + firstname + ",");
				try {
					DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
							.cacheOnDisc(true).cacheInMemory(true)
							.imageScaleType(ImageScaleType.EXACTLY)
							.displayer(new FadeInBitmapDisplayer(300)).build();

					ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
							getApplicationContext())
							.defaultDisplayImageOptions(defaultOptions)
							.memoryCache(new WeakMemoryCache())
							.discCacheSize(100 * 1024 * 1024).build();

					ImageLoader.getInstance().init(config);
					ImageLoader imageLoader = ImageLoader.getInstance();
					// DisplayImageOptions options = new
					// DisplayImageOptions.Builder()
					// .cacheInMemory(true).cacheOnDisc(true)
					// .resetViewBeforeLoading(true)
					// .showImageForEmptyUri(R.drawable.user_image_default)
					// .showImageOnFail(R.drawable.user_image_default)
					// .showImageOnLoading(R.drawable.user_image_default).build();

					imageLoader.displayImage(personPhotoUrl + "0", profile_pic);
					dialog.dismiss();

				} catch (Exception e) {
					Toast.makeText(getApplicationContext(),
							"Something went wrong", Toast.LENGTH_SHORT).show();
					// progressView.setVisibility(View.GONE);
					dialog.dismiss();
				}

			} else {
				Toast.makeText(getApplicationContext(),
						"Person information is null", Toast.LENGTH_SHORT)
						.show();
				// progressView.setVisibility(View.GONE);
				dialog.dismiss();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressLint("DefaultLocale")
	public void getinfo() {
		Session session = Session.getActiveSession();
		if (session.isOpened()) {
			Request.newMeRequest(session, new Request.GraphUserCallback() {
				@Override
				public void onCompleted(GraphUser user, Response response) {
					if (user != null) {
						Log.d("name", user.getName() + "");
						Log.d("id", user.getId() + "");
						Log.d("email",
								response.getGraphObject().getProperty("email")
										+ "");
						nameperson = user.getName().toString();
						name.setText(nameperson);
						email.setText(emailperson);
						SharedPreferences preferences = PreferenceManager
								.getDefaultSharedPreferences(Registration.this);
						SharedPreferences.Editor editor = preferences.edit();
						editor.putString("name", nameperson);
						editor.putString("email", emailperson);
						editor.putString("progress", "registration");
						editor.putString("logintype", "facebook");
						editor.putString("photourl",
								"http://graph.facebook.com/"
										+ user.getId().toString()
										+ "/picture?width=150");
						PHOTO="http://graph.facebook.com/"
										+ user.getId().toString()
										+ "/picture?width=150";
						editor.apply();
						String firstname = nameperson.substring(0,
								nameperson.indexOf(' '));
						firstname = firstname.substring(0, 1).toUpperCase()
								+ firstname.substring(1);
						hiuser.setText("Hi " + firstname + ",");
						try {
							DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
									.cacheOnDisc(true).cacheInMemory(true)
									.imageScaleType(ImageScaleType.EXACTLY)
									.displayer(new FadeInBitmapDisplayer(300))
									.build();

							ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
									getApplicationContext())
									.defaultDisplayImageOptions(defaultOptions)
									.memoryCache(new WeakMemoryCache())
									.discCacheSize(100 * 1024 * 1024).build();

							ImageLoader.getInstance().init(config);
							ImageLoader imageLoader = ImageLoader.getInstance();
							// DisplayImageOptions options = new
							// DisplayImageOptions.Builder()
							// .cacheInMemory(true).cacheOnDisc(true)
							// .resetViewBeforeLoading(true)
							// .showImageForEmptyUri(R.drawable.user_image_default)
							// .showImageOnFail(R.drawable.user_image_default)
							// .showImageOnLoading(R.drawable.user_image_default).build();

							imageLoader
									.displayImage("http://graph.facebook.com/"
											+ user.getId().toString()
											+ "/picture?width=150", profile_pic);
							Log.i("link", "http://graph.facebook.com/"
									+ user.getId().toString() + "/picture");
							// progressView.setVisibility(View.GONE);
							dialog.dismiss();

						} catch (Exception e) {
							Toast.makeText(getApplicationContext(),
									"Something went wrong", Toast.LENGTH_SHORT)
									.show();
							progressView.setVisibility(View.GONE);
							// http://graph.facebook.com/ user.getid() /picture
							// email
							dialog.dismiss();
						}
					}
				}
			}).executeAsync();

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		}
		return super.onOptionsItemSelected(item);
	}

	public static void callFacebookLogout(Context context) {

		Session session = Session.getActiveSession();
		if (session != null) {

			if (!session.isClosed()) {
				session.closeAndClearTokenInformation();
				// clear your preferences if saved
			}
		} else {
			session = new Session(context);
			Session.setActiveSession(session);
			session.closeAndClearTokenInformation();
			// clear your preferences if saved
		}
	}

	private void signOutFromGplus() {
		if (mGoogleApiClient.isConnected()) {
			Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
			mGoogleApiClient.disconnect();
			mGoogleApiClient.connect();
			finish();
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub
		if (!result.hasResolution()) {
			GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
					0).show();
			return;
		}

		if (!mIntentInProgress) {
			// Store the ConnectionResult for later usage
			mConnectionResult = result;

			if (mSignInClicked) {
				// The user has already clicked 'sign-in' so we attempt to
				// resolve all
				// errors until the user is signed in, or they cancel.
				resolveSignInError();
			}
		}

	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		mSignInClicked = false;
		getProfileInformation();

		// Toast.makeText(this, "User is connected!",
		// Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		mGoogleApiClient.connect();

	}

	private void resolveSignInError() {
		if (mConnectionResult.hasResolution()) {
			try {
				mIntentInProgress = true;
				mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
			} catch (SendIntentException e) {
				mIntentInProgress = false;
				mGoogleApiClient.connect();
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
		if (requestCode == RC_SIGN_IN) {
			if (resultCode != RESULT_OK) {
				mSignInClicked = false;
			}

			mIntentInProgress = false;

			if (!mGoogleApiClient.isConnecting()) {
				mGoogleApiClient.connect();
			}
		}
	}

	protected void onStop() {
		super.onStop();
		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
	}

	protected void onStart() {
		super.onStart();
		mGoogleApiClient.connect();
	}

	@Override
	public void onBackPressed() {

		Log.i("logintype", logintype);

		if (logintype.equals("google") || logintype.equals("facebook")) {
			new LogoutfromServer().execute();

		} else {
			finish();
			super.onBackPressed();
		}

	}

	private class LogoutfromServer extends AsyncTask<String, String, String> {
		Dialog dialog;
		String accessToken;
		CircularProgressView progressView;
		TextView progress_text;
		String result, url;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// custom dialog
			dialog = new Dialog(Registration.this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.custom_dialog);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			progressView = (CircularProgressView) dialog
					.findViewById(R.id.progressView);
			progress_text = (TextView) dialog.findViewById(R.id.progresstext);
			progress_text.setText("Signing Out..");
			progress_text.setTypeface(type);
			com.Material.Utilities.startAnimationThreadStuff(500, progressView);
			progressView.setIndeterminate(true);
			dialog.show();
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(getApplicationContext());
			accessToken = preferences.getString("accessToken", "");
			url = "http://app.liquamart.com/api/users/logout?access_token="
					+ accessToken;
			Log.i("logoutapi", url);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg) {
			// do above Server call here
			result = getJsonData(url);
			Log.i("result", result);
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// process message
			// Toast.makeText(getApplicationContext(), result,
			// Toast.LENGTH_LONG).show();
			dialog.dismiss();
			Log.i("result", result);
			signOutFromGplus();
			callFacebookLogout(getApplicationContext());
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(getApplicationContext());
			SharedPreferences.Editor editor = preferences.edit();
			editor.putString("name", "");
			editor.putString("email", "");
			editor.putString("progress", "login");
			editor.putString("logintype", "");
			editor.putString("photourl", "");
			editor.putString("accessToken", "");
			editor.putString("userid", "");
			editor.putString("adapterprogress", "");
			editor.putString("state","" );
			editor.putString("phone","" );
			editor.apply();
			Intent i = new Intent(Registration.this, Login.class);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(i);
			finish();

		}
	}

	public String getJsonData(String url) {

		StringBuilder builder = new StringBuilder();

		HttpClient client = new DefaultHttpClient();

		HttpPost httpGet = new HttpPost(url);

		try {

			HttpResponse response = client.execute(httpGet);

			StatusLine statusLine = response.getStatusLine();

			int statusCode = statusLine.getStatusCode();

			if (statusCode == 200) {

				HttpEntity entity = response.getEntity();

				InputStream content = entity.getContent();

				BufferedReader reader = new BufferedReader(

				new InputStreamReader(content));

				String line;

				while ((line = reader.readLine()) != null) {

					builder.append(line);

				}

			} else {

				// System.out.print("condition not true");

				// Log.e(ParseJSON.class.toString(),
				// "Failed to download file");

			}

		} catch (ClientProtocolException e) {

			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		}

		return builder.toString();

	}

}
