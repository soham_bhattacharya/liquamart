package com.liquamart;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.Material.CircularProgressView;
import com.Material.ConnectionDetector;
import com.Material.PullRefreshLayout;
import com.categories.tabs.MyAdapterMyProducts;
import com.facebook.Session;
import com.facebook.UiLifecycleHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;
import com.products.MyProductAdapter;
import com.products.MyProductSetter;
import com.products.ProductAdapter;

public class MyProducts extends ActionBarActivity implements
		ConnectionCallbacks, OnConnectionFailedListener {

	// First We Declare Titles And Icons For Our Navigation Drawer List View
	// This Icons And Titles Are holded in an Array as you can see

	String TITLES[] = { "Home", "My Products", "Favorites", 
			"Order Replacement", "FAQ's", "Contact Support",
			"Profile Settings", "Logout" };
	int ICONS[] = { R.drawable.icon_home, R.drawable.icon_bproducts,
			R.drawable.icon_favourite, R.drawable.icon_replacement,
			R.drawable.icon_faqs, R.drawable.icon_contact,
			R.drawable.icon_profileset, R.drawable.icon_logout};
	String NAME, EMAIL, PHOTO;
	private Toolbar toolbar; // Declaring the Toolbar Object
	RecyclerView mRecyclerView; // Declaring RecyclerView
	RecyclerView.Adapter mAdapter; // Declaring Adapter For Recycler View
	RecyclerView.LayoutManager mLayoutManager; // Declaring Layout Manager as a
												// linear layout manager
	public static DrawerLayout Drawer; // Declaring DrawerLayout
	ActionBarDrawerToggle mDrawerToggle; // Declaring Action Bar Drawer Toggle

	private static GoogleApiClient mGoogleApiClient;
	private UiLifecycleHelper uiHelper;
	private static final int RC_SIGN_IN = 0;
	private boolean mSignInClicked;
	private ConnectionResult mConnectionResult;
	// private SignInButton btnSignIn;
	private boolean mIntentInProgress;
	public static Activity activity = null;
	Typeface type;
	public String[] names;
	public String[] imageurls;
	public String[] dates;
	public String[] productids;
	ListView listView;
	List<MyProductSetter> rowItems;
	SwipeRefreshLayout mSwipeRefreshLayout;
	TextView msg;
	Button addproduct;
	Boolean isInternetPresent = false;
	ConnectionDetector cd;
	PullRefreshLayout layout;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.myproducts);
		activity = this;
		cd = new ConnectionDetector(getApplicationContext());
		type = Typeface.createFromAsset(getAssets(),
				"fonts/sanfracisco_regular.ttf");
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(MyProducts.this);
		String NAME = preferences.getString("name", "");
		String EMAIL = preferences.getString("email", "");
		String PHOTO = preferences.getString("photourl", "");
		addproduct=(Button) findViewById(R.id.add_product);
		
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(Plus.API, Plus.PlusOptions.builder().build())
				.addScope(Plus.SCOPE_PLUS_LOGIN).build();

		msg=(TextView) findViewById(R.id.noproducts_msg);
		msg.setTypeface(type);
		/*
		 * Assinging the toolbar object ot the view and setting the the Action
		 * bar to our toolbar
		 */
		toolbar = (Toolbar) findViewById(R.id.tool_bar);
		toolbar.setTitle("My Products");
		setSupportActionBar(toolbar);
		

		mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning
																		// the
																		// RecyclerView
																		// Object
																		// to
																		// the
																		// xml
																		// View

		mRecyclerView.setHasFixedSize(true); // Letting the system know that the
												// list objects are of fixed
												// size

		mAdapter = new MyAdapterMyProducts(getApplicationContext(), activity, TITLES,
				ICONS, NAME, EMAIL, PHOTO); // Creating the Adapter of MyAdapter
											// class(which
											// we are going to see in a bit)
		// And passing the titles,icons,header view name, header view email,
		// and header view profile picture

		mRecyclerView.setAdapter(mAdapter); // Setting the adapter to
											// RecyclerView

		mLayoutManager = new LinearLayoutManager(this); // Creating a layout
														// Manager

		mRecyclerView.setLayoutManager(mLayoutManager); // Setting the layout
														// Manager

		Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout); // Drawer
																	// object
																	// Assigned
																	// to the
																	// view
		mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, toolbar,
				R.string.openDrawer, R.string.closeDrawer) {

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				// code here will execute once the drawer is opened( As I dont
				// want anything happened whe drawer is
				// open I am not going to put anything here)
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				// Code here will execute once drawer is closed
			}

		}; // Drawer Toggle Object Made
		Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the
													// Drawer toggle
		mDrawerToggle.syncState(); // Finally we set the drawer toggle sync
									// State
		isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
		new GetProducts().execute();
        }
        else
        {
        	showAlertDialog(MyProducts.this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
//		mSwipeRefreshLayout=(SwipeRefreshLayout)findViewById(R.id.swipeRefreshLayout);
//		mSwipeRefreshLayout.setColorSchemeResources(R.color.blue,R.color.orange,R.color.green);
//		mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {  
//		    @Override
//		    public void onRefresh() {
//		        // Refresh items
//		    	isInternetPresent = cd.isConnectingToInternet();
//		        if (isInternetPresent) {
//		        new GetProductsRefresh().execute();
//		        mSwipeRefreshLayout.setRefreshing(false);
//		        }
//		        else
//		        {
//		        	showAlertDialog(MyProducts.this, "No Internet Connection",
//		                    "You don't have internet connection.", false);
//		        }
//		    }
//		});
        layout = (PullRefreshLayout)findViewById(R.id.swipeRefreshLayout);
		  layout.setRefreshStyle(PullRefreshLayout.STYLE_WATER_DROP);
		// listen refresh event
		layout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
		    @Override
		    public void onRefresh() {
		        // start refresh
		    	isInternetPresent = cd.isConnectingToInternet();
		        if (isInternetPresent) {
		        new GetProductsRefresh().execute();
		        layout.setRefreshing(false);
		        }
		        else
		        {
		        	showAlertDialog(activity, "No Internet Connection",
		                    "You don't have internet connection.", false);
		        	layout.setRefreshing(false);
		        }
		        
		    }
		});

        
		addproduct.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i = new Intent("com.liquamart.ScanQRCode");
				startActivity(i);
				finish();
			}
		});
	}
	
	public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
 
        // Setting Dialog Title
        alertDialog.setTitle(title);
 
        // Setting Dialog Message
        alertDialog.setMessage(message);
         
        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.app_icon : R.drawable.app_icon);
 
        // Showing Alert Message
        alertDialog.show();
    }
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		if (Drawer.isDrawerOpen(Gravity.LEFT)) {
			Drawer.closeDrawer(Gravity.LEFT);
		} else {
			finish();
			super.onBackPressed();
		}

	}

	public static void callFacebookLogout(Context context) {

		Session session = Session.getActiveSession();
		if (session != null) {

			if (!session.isClosed()) {
				session.closeAndClearTokenInformation();
				// Toast.makeText(context, "Logged out from facebook",
				// Toast.LENGTH_SHORT).show();
				// clear your preferences if saved
			}

		} else {
			session = new Session(context);
			Session.setActiveSession(session);
			session.closeAndClearTokenInformation();
			// clear your preferences if saved
		}
	}

	public static void signOutFromGplus() {
		if (mGoogleApiClient.isConnected()) {

			Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
			mGoogleApiClient.disconnect();
			mGoogleApiClient.connect();
			// Toast.makeText(activity, "Logged out from google",
			// Toast.LENGTH_SHORT).show();
			activity.finish();
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub
		if (!result.hasResolution()) {
			GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
					0).show();
			return;
		}

		if (!mIntentInProgress) {
			// Store the ConnectionResult for later usage
			mConnectionResult = result;

			if (mSignInClicked) {
				// The user has already clicked 'sign-in' so we attempt to
				// resolve all
				// errors until the user is signed in, or they cancel.
				resolveSignInError();
			}
		}

	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		mSignInClicked = false;
		// Toast.makeText(this, "User is connected!",
		// Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		mGoogleApiClient.connect();

	}

	private void resolveSignInError() {
		if (mConnectionResult.hasResolution()) {
			try {
				mIntentInProgress = true;
				mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
			} catch (SendIntentException e) {
				mIntentInProgress = false;
				mGoogleApiClient.connect();
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
		if (requestCode == RC_SIGN_IN) {
			if (resultCode != RESULT_OK) {
				mSignInClicked = false;
			}

			mIntentInProgress = false;

			if (!mGoogleApiClient.isConnecting()) {
				mGoogleApiClient.connect();
			}
		}
	}

	protected void onStop() {
		super.onStop();
		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
	}

	protected void onStart() {
		super.onStart();
		mGoogleApiClient.connect();
	}
	
	private class GetProducts extends AsyncTask<String, String, String> {
		Dialog dialog;
		String accessToken;
		CircularProgressView progressView;
		TextView progress_text;
		String result, id,myproductsurl;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// custom dialog
			dialog = new Dialog(activity);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.custom_dialog);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			progressView = (CircularProgressView) dialog
					.findViewById(R.id.progressView);
			progress_text = (TextView) dialog.findViewById(R.id.progresstext);
			progress_text.setText("Searching products..");
			progress_text.setTypeface(type);
			com.Material.Utilities.startAnimationThreadStuff(500, progressView);
			progressView.setIndeterminate(true);
			dialog.show();
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(activity);
			accessToken = preferences.getString("accessToken", "");
			id = preferences.getString("userid", "");
			Log.i("id", id);
			myproductsurl = "http://app.liquamart.com/api/users/" + id
					+ "/purchases?access_token=" + accessToken;
			Log.i("myproductsurl", myproductsurl);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg) {
			// do above Server call here
			result = getJsonDataget(myproductsurl);
			Log.i("result", result);
			try {
				JSONArray products=new JSONArray(result);
				names=new String[products.length()];
				dates=new String[products.length()];
				productids=new String[products.length()];
				imageurls=new String[products.length()];
				for(int i=0;i<products.length();i++)
				{
					JSONObject o=products.getJSONObject(i);
					productids[i]=o.getString("productId");
					names[i]=o.getString("name");
					dates[i]=o.getString("date");
					imageurls[i]=o.getString("imgUrl");
					
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// process message
			// Toast.makeText(getApplicationContext(), result,
			// Toast.LENGTH_LONG).show();
			dialog.dismiss();
			Log.i("result", result);
			rowItems = new ArrayList<MyProductSetter>();
	        for (int i = 0; i < names.length; i++) {
	            MyProductSetter item = new MyProductSetter(productids[i],names[i], imageurls[i], dates[i]);
	            rowItems.add(item);
	        }
	        listView = (ListView) findViewById(R.id.list);
	        MyProductAdapter adapter = new MyProductAdapter(getApplicationContext(), rowItems);
	        listView.setAdapter(adapter);
	        if(productids.length==0)
	        {
	        	listView.setVisibility(View.GONE);
	        	msg.setVisibility(View.VISIBLE);
	        	addproduct.setVisibility(View.VISIBLE);
	        }
	        else
	        {
	        	listView.setVisibility(View.VISIBLE);
	        	addproduct.setVisibility(View.VISIBLE);
	        }
			// Intent i=new Intent(con,Login.class);
			// i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
			// Intent.FLAG_ACTIVITY_CLEAR_TASK);
			// con.startActivity(i);
			// activity.finish();

		}
	}
	
	private class GetProductsRefresh extends AsyncTask<String, String, String> {
		Dialog dialog;
		String accessToken;
		CircularProgressView progressView;
		TextView progress_text;
		String result, id,myproductsurl;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// custom dialog
		
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(activity);
			accessToken = preferences.getString("accessToken", "");
			id = preferences.getString("userid", "");
			Log.i("id", id);
			myproductsurl = "http://app.liquamart.com/api/users/" + id
					+ "/purchases?access_token=" + accessToken;
			Log.i("myproductsurl", myproductsurl);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg) {
			// do above Server call here
			result = getJsonDataget(myproductsurl);
			try {
				JSONArray products=new JSONArray(result);
				names=new String[products.length()];
				dates=new String[products.length()];
				productids=new String[products.length()];
				imageurls=new String[products.length()];
				for(int i=0;i<products.length();i++)
				{
					JSONObject o=products.getJSONObject(i);
					productids[i]=o.getString("productId");
					names[i]=o.getString("name");
					dates[i]=o.getString("date");
					imageurls[i]=o.getString("imgUrl");
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// process message
			// Toast.makeText(getApplicationContext(), result,
			// Toast.LENGTH_LONG).show();
			Log.i("result", result);
			rowItems = new ArrayList<MyProductSetter>();
	        for (int i = 0; i < names.length; i++) {
	            MyProductSetter item = new MyProductSetter(productids[i],names[i], imageurls[i], dates[i]);
	            rowItems.add(item);
	        }
	        listView = (ListView) findViewById(R.id.list);
	        ProductAdapter adapter = new ProductAdapter(getApplicationContext(), rowItems);
	        listView.setAdapter(adapter);
	        if(productids.length==0)
	        {
	        	listView.setVisibility(View.GONE);
	        	msg.setVisibility(View.VISIBLE);
	        }
			// Intent i=new Intent(con,Login.class);
			// i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
			// Intent.FLAG_ACTIVITY_CLEAR_TASK);
			// con.startActivity(i);
			// activity.finish();

		}
	}

	public String getJsonDataget(String url) {

		StringBuilder builder = new StringBuilder();

		HttpClient client = new DefaultHttpClient();

		HttpGet httpGet = new HttpGet(url);

		try {

			HttpResponse response = client.execute(httpGet);

			StatusLine statusLine = response.getStatusLine();

			int statusCode = statusLine.getStatusCode();

			if (statusCode == 200) {

				HttpEntity entity = response.getEntity();

				InputStream content = entity.getContent();

				BufferedReader reader = new BufferedReader(

				new InputStreamReader(content));

				String line;

				while ((line = reader.readLine()) != null) {

					builder.append(line);

				}

			} else {

				// System.out.print("condition not true");

				// Log.e(ParseJSON.class.toString(),
				// "Failed to download file");

			}

		} catch (ClientProtocolException e) {

			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		}

		return builder.toString();

	}
	
}