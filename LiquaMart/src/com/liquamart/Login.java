package com.liquamart;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.Material.CircularProgressView;
import com.Material.ConnectionDetector;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.facebook.Session;
import com.facebook.SessionState;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

@SuppressWarnings("deprecation")
public class Login extends ActionBarActivity implements ConnectionCallbacks,
		BaseSliderView.OnSliderClickListener, OnConnectionFailedListener,
		ViewPagerEx.OnPageChangeListener {
	ActionBar ab;
	LinearLayout facebook;
	LinearLayout google;
	// private LoginButton loginBtn;
	private static Session session = null;
	public GoogleApiClient mGoogleApiClient;
	private boolean mIntentInProgress;
	Button signup, fb_login, google_login;
	private static final int RC_SIGN_IN = 0;
	private boolean mSignInClicked;
	private ConnectionResult mConnectionResult;
	private SliderLayout mDemoSlider;
	DefaultHttpClient httpclient;
	HttpPost httppost;
	ArrayList<NameValuePair> nameValuePairs;
	String token, result, result2;
	boolean forResult = false;
	CircularProgressView progressView;
	TextView progress_text;
	String name = "", email = "";
	Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(
			this, Arrays.asList("public_profile,email,user_birthday"));
	Typeface type;
	Dialog dialog;
	String googleid, googlename, googelemail;
	// flag for Internet connection status
		Boolean isInternetPresent = false;
		ConnectionDetector cd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ab = getSupportActionBar();
		ab.hide();
		setContentView(R.layout.login);
		signup = (Button) findViewById(R.id.signup);
		fb_login = (Button) findViewById(R.id.login_fb);

		google_login = (Button) findViewById(R.id.login_google);
		 cd = new ConnectionDetector(getApplicationContext());
		signup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startActivity(new Intent("com.liquamart.LoginwithEmail"));
			}
		});
		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					"com.liquamart", PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.e("MY KEY HASH:",
						Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {

		} catch (NoSuchAlgorithmException e) {

		}

		// facebook login coding

		facebook = (LinearLayout) findViewById(R.id.layout_fb);
		facebook.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				 // get Internet status
                isInternetPresent = cd.isConnectingToInternet();
 
                // check for Internet status
                if (isInternetPresent) {
				forResult = false;

				Session s = new Session(Login.this);
				Session.setActiveSession(s);
				Session.OpenRequest request = new Session.OpenRequest(
						Login.this);
				request.setPermissions(Arrays.asList("public_profile", "email"));
				request.setCallback(new Session.StatusCallback() {
					// callback when session changes state
					@Override
					public void call(Session session, SessionState state,
							Exception exception) {
						if (session.isOpened()) {
							// getinfo();
							token = session.getAccessToken() + "";
							Log.i("token", token);
							// Toast.makeText(getApplicationContext(),
							// session.getAccessToken(),
							// Toast.LENGTH_SHORT).show();

							new NodeResponse().execute();
							// Intent i = new Intent(
							// "com.liquamart.Registration");
							// i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							// i.putExtra("login", "facebook");
							// startActivity(i);
						}
					}
				}); // end of call;

				s.openForRead(request); // now do the request above
			}
			else
			{
				showAlertDialog(Login.this, "No Internet Connection",
                        "You don't have internet connection.", false);
			}
			}
		});

		fb_login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				 isInternetPresent = cd.isConnectingToInternet();
	                if (isInternetPresent) {
				forResult = false;

				Session s = new Session(Login.this);
				Session.setActiveSession(s);
				Session.OpenRequest request = new Session.OpenRequest(
						Login.this);
				request.setPermissions(Arrays.asList("public_profile", "email"));
				request.setCallback(new Session.StatusCallback() {
					// callback when session changes state
					@Override
					public void call(Session session, SessionState state,
							Exception exception) {
						if (session.isOpened()) {
							// getinfo();
							token = session.getAccessToken() + "";
							Log.i("token", token);
							// Toast.makeText(getApplicationContext(),
							// session.getAccessToken(),
							// Toast.LENGTH_SHORT).show();

							new NodeResponse().execute();
							// Intent i = new Intent(
							// "com.liquamart.Registration");
							// i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							// i.putExtra("login", "facebook");
							// startActivity(i);
						}
					}
				}); // end of call;

				s.openForRead(request); // now do the request above

			}
			
			else
			{
				showAlertDialog(Login.this, "No Internet Connection",
                        "You don't have internet connection.", false);
			}
			}
		});

		// google login coding
		google = (LinearLayout) findViewById(R.id.layout_google);
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(Plus.API, Plus.PlusOptions.builder().build())
				.addScope(
						new Scope(
								"https://www.googleapis.com/auth/userinfo.email"))
				.addScope(Plus.SCOPE_PLUS_LOGIN).build();
		google.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				 isInternetPresent = cd.isConnectingToInternet();
	                if (isInternetPresent) {
				forResult = true;
				dialog = new Dialog(Login.this);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.custom_dialog);
				dialog.setCanceledOnTouchOutside(true);
				dialog.setCancelable(true);

				progressView = (CircularProgressView) dialog
						.findViewById(R.id.progressView);
				progress_text = (TextView) dialog
						.findViewById(R.id.progresstext);
				progress_text.setTypeface(type);
				com.Material.Utilities.startAnimationThreadStuff(500,
						progressView);
				progressView.setIndeterminate(true);
				dialog.show();
				signInWithGplus();
			}
	                else 
	                {
	                	showAlertDialog(Login.this, "No Internet Connection",
	                            "You don't have internet connection.", false);
	                }
			}
		});

		google_login.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
				forResult = true;
				dialog = new Dialog(Login.this);
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.custom_dialog);
				dialog.setCanceledOnTouchOutside(false);
				dialog.setCancelable(false);
				progressView = (CircularProgressView) dialog
						.findViewById(R.id.progressView);
				progress_text = (TextView) dialog
						.findViewById(R.id.progresstext);
				progress_text.setTypeface(type);
				com.Material.Utilities.startAnimationThreadStuff(500,
						progressView);
				progressView.setIndeterminate(true);
				dialog.show();
				signInWithGplus();
			}
                else
                {
                	showAlertDialog(Login.this, "No Internet Connection",
                            "You don't have internet connection.", false);
                }
			}
		});
		/*
		 * loginBtn = (LoginButton) findViewById(R.id.fb_login_button);
		 * loginBtn.setReadPermissions(Arrays.asList("email"));
		 * loginBtn.setUserInfoChangedCallback(new UserInfoChangedCallback() {
		 * 
		 * @Override public void onUserInfoFetched(GraphUser user) { if (user !=
		 * null) { username.setText("You are currently logged in as " +
		 * user.getName()); getinfo(); startActivity(new
		 * Intent("com.liquamart.Question")); } else {
		 * username.setText("You are not logged in."); } } });
		 */

		// slider coding
		// changes in defaultsliderview.java
		mDemoSlider = (SliderLayout) findViewById(R.id.slider);
		HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
		file_maps.put("3", R.drawable.slider2);
		file_maps.put("2", R.drawable.slider3);
		file_maps.put("5", R.drawable.slider4);
		file_maps.put("4", R.drawable.slider5);
		

		for (String name : file_maps.keySet()) {
			DefaultSliderView textSliderView = new DefaultSliderView(this);
			// initialize a SliderLayout
			textSliderView.image(file_maps.get(name)).setScaleType(
					BaseSliderView.ScaleType.FitCenterCrop);
			mDemoSlider.addSlider(textSliderView);
		}
		mDemoSlider
				.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
		mDemoSlider.setCustomAnimation(new DescriptionAnimation());

		mDemoSlider.setDuration(3500);

		type = Typeface.createFromAsset(getAssets(),
				"fonts/sanfracisco_regular.ttf");
		fb_login.setTypeface(type);
		google_login.setTypeface(type);
		signup.setTypeface(type);

	}

	private class NodeResponse extends AsyncTask<String, String, String> {
		Dialog dialog;
		String email, accessToken,userid,name2,contact2,state2,email2,photourl;
		Boolean firsttime=false;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// custom dialog
			dialog = new Dialog(Login.this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.custom_dialog);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			progressView = (CircularProgressView) dialog
					.findViewById(R.id.progressView);
			progress_text = (TextView) dialog.findViewById(R.id.progresstext);
			progress_text.setTypeface(type);
			com.Material.Utilities.startAnimationThreadStuff(500, progressView);
			progressView.setIndeterminate(true);
			dialog.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg) {
			// do above Server call here
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://app.liquamart.com/api/users/loginWithFb");

			try {
				// Add your data
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						2);
				nameValuePairs
						.add(new BasicNameValuePair("accessToken", token));
				// httppost.setHeader(HTTP.CONTENT_TYPE,
				// "application/x-www-form-urlencoded;charset=UTF-8");
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();

				InputStream in = entity.getContent();

				BufferedReader br = new BufferedReader(new InputStreamReader(
						in, "UTF-8"), 8);

				StringBuilder sb = new StringBuilder();
				String line = null;

				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");

				}
				result = sb.toString();
				try {
					JSONObject rs = new JSONObject(result);
					email = rs.getString("email");
					accessToken = rs.getString("accessToken");
					userid = rs.getString("userId");
					Log.d("accessToken", accessToken);
					
					try{
						firsttime=false;
						JSONObject ob=rs.getJSONObject("user");
						name2 = ob.getString("fname");
						 contact2 = ob.getString("contact");
						 state2 = ob.getString("state");
						 email2 = ob.getString("email");
						 photourl = ob.getString("photourl");
					}
					catch(Exception e)
					{
						firsttime=true;
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// process message
			// Toast.makeText(getApplicationContext(), result,
			// Toast.LENGTH_LONG).show();
			
//			Log.i("result", result);
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(Login.this);
			SharedPreferences.Editor editor = preferences.edit();
			if(firsttime==true){
			editor.putString("accessToken", accessToken);
			editor.putString("userid", userid);
			editor.putString("logintype", "facebook");
			editor.apply();
			dialog.dismiss();
			Intent i = new Intent("com.liquamart.Registration");
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			i.putExtra("login", "facebook");
			i.putExtra("email", email);
			startActivity(i);
			finish();
			}
			else if(firsttime==false)
			{
//				getinfo();
				editor.putString("accessToken", accessToken);
				editor.putString("userid", userid);
				editor.putString("name", name2);
				editor.putString("state", state2);
				editor.putString("phone", contact2);
				editor.putString("email", email2);
				editor.putString("logintype", "facebook");
				editor.putString("progress", "main");
				editor.putString("photourl", photourl);
				editor.apply();
				dialog.dismiss();
				Intent i = new Intent("com.liquamart.Categories");
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
				finish();
			}
		}
	}
	
	
	

	protected void onStart() {
		super.onStart();
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(Login.this);
		String progress = preferences.getString("progress", "");
		if(progress.equals("main"))
		{
//			Toast.makeText(getApplicationContext(), "dsfsdfsdf", Toast.LENGTH_SHORT).show();
		}
		else
		mGoogleApiClient.connect();
	}

		/*
	 * private Session.StatusCallback statusCallback = new
	 * Session.StatusCallback() {
	 * 
	 * @Override public void call(Session session, SessionState state, Exception
	 * exception) { // TODO Auto-generated method stub if (state.isOpened()) {
	 * Log.d("MainActivity", "Facebook session opened."); } else if
	 * (state.isClosed()) { Log.d("MainActivity", "Facebook session closed."); }
	 * } };
	 */

	protected void onStop() {
		super.onStop();
		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
		mDemoSlider.stopAutoCycle();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (forResult == true) {
			// google session coding
			if (requestCode == RC_SIGN_IN) {
				// progressView.setVisibility(View.VISIBLE);
				// com.Material.Utilities.startAnimationThreadStuff(500,
				// progressView);
				// progressView.setIndeterminate(true);
				dialog.dismiss();

				if (resultCode != RESULT_OK) {
					mSignInClicked = false;
				}
				mIntentInProgress = false;
				if (!mGoogleApiClient.isConnecting()) {
					mGoogleApiClient.connect();
				}
			}
		}
		// faceebook session coding
		if (forResult == false) {
			Session.getActiveSession().onActivityResult(this, requestCode,
					resultCode, data);
			// progressView.setVisibility(View.VISIBLE);
			// com.Material.Utilities.startAnimationThreadStuff(500,
			// progressView);
			// progressView.setIndeterminate(true);

		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub
		if (!result.hasResolution()) {
			GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
					0).show();

			return;
		}
		// progressView.setVisibility(View.GONE);
		if (!mIntentInProgress) {
			// Store the ConnectionResult for later usage
			mConnectionResult = result;

			if (mSignInClicked) {
				// The user has already clicked 'sign-in' so we attempt to
				// resolve all
				// errors until the user is signed in, or they cancel.
				resolveSignInError();
			}
		}

	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		mSignInClicked = false;
		// Toast.makeText(this, "User is connected!",
		// Toast.LENGTH_SHORT).show();
		// Toast.makeText(this, token, Toast.LENGTH_SHORT).show();
		// Get user's information
		getProfileInformation();

	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		mGoogleApiClient.connect();

	}

	private void signInWithGplus() {
		if (!mGoogleApiClient.isConnecting()) {
			mSignInClicked = true;
			resolveSignInError();
		}
	}

	private void resolveSignInError() {
		if (mConnectionResult.hasResolution()) {
			try {
				mIntentInProgress = true;
				mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
			} catch (SendIntentException e) {
				mIntentInProgress = false;
				mGoogleApiClient.connect();
			}
		}
	}

	private void getProfileInformation() {
		try {
			if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {

				Person currentPerson = Plus.PeopleApi
						.getCurrentPerson(mGoogleApiClient);
				String personName = currentPerson.getDisplayName();
				String id = currentPerson.getId();
				googleid = id;
				// String scopes = "oauth2:" + Scopes.PLUS_LOGIN + " " +
				// Scopes.EMAIL;
				String personPhotoUrl = currentPerson.getImage().getUrl();
				String personGooglePlusProfile = currentPerson.getUrl();
				String email1 = Plus.AccountApi
						.getAccountName(mGoogleApiClient);
				Log.e("Info", "Name: " + personName + ", plusProfile: "
						+ personGooglePlusProfile + ", email: " + email1
						+ ", Image: " + personPhotoUrl + "gender "
						+ currentPerson.getGender() + " location"
						+ currentPerson.getCurrentLocation() + "id" + id);
				name = currentPerson.getDisplayName();
				email = Plus.AccountApi.getAccountName(mGoogleApiClient);
				googelemail = email;
				googlename = name;
				new NodeResponseGoogle().execute();

			} else {
				Toast.makeText(getApplicationContext(),
						"Person information is null", Toast.LENGTH_SHORT)
						.show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private class NodeResponseGoogle extends AsyncTask<String, String, String> {
		String accessToken,userid,name2,contact2,state2,email2,photourl;
		Boolean firsttime=false;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// custom dialog
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg) {
			// do above Server call here
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://app.liquamart.com/api/users/loginWithGoogle");

			try {
				// Add your data
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						2);
				nameValuePairs.add(new BasicNameValuePair("uid", googleid));
				nameValuePairs
						.add(new BasicNameValuePair("email", googelemail));
				nameValuePairs.add(new BasicNameValuePair("name", googlename));
				// httppost.setHeader(HTTP.CONTENT_TYPE,
				// "application/x-www-form-urlencoded;charset=UTF-8");
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();

				InputStream in = entity.getContent();

				BufferedReader br = new BufferedReader(new InputStreamReader(
						in, "UTF-8"), 8);

				StringBuilder sb = new StringBuilder();
				String line = null;

				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");

				}
				result = sb.toString();
				try {
					JSONObject rs = new JSONObject(result);
					accessToken = rs.getString("accessToken");
					userid = rs.getString("userId");
					Log.d("accessToken", accessToken);
					
					try{
						firsttime=false;
						JSONObject ob=rs.getJSONObject("user");
						name2 = ob.getString("fname");
						 contact2 = ob.getString("contact");
						 state2 = ob.getString("state");
						 email2 = ob.getString("email");
						 photourl = ob.getString("photourl");
					}
					catch(Exception e)
					{
						firsttime=true;
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// process message
			// Toast.makeText(getApplicationContext(), result,
			// Toast.LENGTH_LONG).show();
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(Login.this);
			SharedPreferences.Editor editor = preferences.edit();
			if(firsttime==true){
			editor.putString("accessToken", accessToken);
			editor.putString("userid", userid);
			editor.putString("logintype", "google");
			editor.apply();
			Intent i = new Intent("com.liquamart.Registration");
			i.putExtra("login", "google");
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			finish();
			}
			else if(firsttime==false)
			{
//				getinfo();
				editor.putString("accessToken", accessToken);
				editor.putString("userid", userid);
				editor.putString("name", name2);
				editor.putString("state", state2);
				editor.putString("phone", contact2);
				editor.putString("email", email2);
				editor.putString("logintype", "google");
				editor.putString("progress", "main");
				editor.putString("photourl", photourl);
				editor.apply();
				Intent i = new Intent("com.liquamart.Categories");
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
				finish();
			}
		}
	}

	@Override
	public void onPageScrolled(int position, float positionOffset,
			int positionOffsetPixels) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageSelected(int position) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageScrollStateChanged(int state) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSliderClick(BaseSliderView slider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume() {
		super.onResume();
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(Login.this);
		String progress = preferences.getString("progress", "");
		String logintype = preferences.getString("logintype", "");
		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed())) {

			if (progress.equals("registration")
					&& progress.equals("registration")) {
				if (logintype.equals("fb")) {
					String email = preferences.getString("email", "");
					Intent i = new Intent("com.liquamart.Registration");
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					i.putExtra("login", "facebook");
					i.putExtra("email", email);
					startActivity(i);
					finish();
				}

			}

		}
		if (logintype.equals("google") && progress.equals("registration")) {
			Intent i = new Intent("com.liquamart.Registration");
			i.putExtra("login", "google");
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			finish();
		}
		if (progress.equals("createaccount")) {
			Intent i = new Intent("com.liquamart.Question");
			startActivity(i);
			finish();
		}
		if (progress.equals("main")) {
			Intent i = new Intent("com.liquamart.Categories");
			startActivity(i);
			finish();
		}
	}
	
	public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
 
        // Setting Dialog Title
        alertDialog.setTitle(title);
 
        // Setting Dialog Message
        alertDialog.setMessage(message);
         
        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.app_icon : R.drawable.app_icon);
 
        // Showing Alert Message
        alertDialog.show();
    }
}
