package com.liquamart;

import com.Material.ConnectionDetector;
import com.Material.MaterialEditText;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class Question extends ActionBarActivity {

	ActionBar ab;
	Button yes, no;
	TextView q;
	Boolean isInternetPresent = false;
	ConnectionDetector cd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.question);
		ab = getSupportActionBar();
		ab.hide();
		cd = new ConnectionDetector(getApplicationContext());
		q=(TextView) findViewById(R.id.question_text);
		yes = (Button) findViewById(R.id.yes_button);
		no = (Button) findViewById(R.id.no_button);
		Typeface type = Typeface.createFromAsset(getAssets(),
				"fonts/sanfracisco_regular.ttf");
		yes.setTypeface(type);
		no.setTypeface(type);
		q.setTypeface(type);
		yes.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i = new Intent("com.liquamart.ScanQRCode");
				startActivity(i);
			}
		});

		no.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
				 startActivity(new Intent("com.liquamart.Categories"));
				 finish();
                }
                else
                {
                	showAlertDialog(Question.this, "No Internet Connection",
                            "You don't have internet connection.", false);
                }
			}
		});
	}
	
	public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
 
        // Setting Dialog Title
        alertDialog.setTitle(title);
 
        // Setting Dialog Message
        alertDialog.setMessage(message);
         
        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.app_icon : R.drawable.app_icon);
 
        // Showing Alert Message
        alertDialog.show();
    }
	
	@Override
	public void onStop()
	{
		super.onStop();
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(Question.this);
		String progress = preferences.getString("progress", "");
		if(progress.equals("facebook")||progress.equals("google")||progress.equals("liqua"))
		{
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString("progress", "main");
		editor.apply();
		}
		
		
	}
}