package com.liquamart;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.Material.CircularProgressView;
import com.Material.ConnectionDetector;
import com.Material.MaterialEditText;

@SuppressWarnings("deprecation")
public class LoginwithEmail extends ActionBarActivity {

	MaterialEditText email,password;
	ActionBar ab;
	Typeface type;
	Boolean isInternetPresent = false;
	ConnectionDetector cd;
	Button login,createaccount;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loginwithemail_fragment);
		ab=getSupportActionBar();
		ab.hide();
		 cd = new ConnectionDetector(getApplicationContext());
		email=(MaterialEditText) findViewById(R.id.text_email);
		password=(MaterialEditText) findViewById(R.id.text_password);
		login=(Button) findViewById(R.id.login);
		createaccount=(Button) findViewById(R.id.donthaveaccount);

		type = Typeface.createFromAsset(getAssets(),
				"fonts/sanfracisco_regular.ttf");
		email.setTypeface(type);
		password.setTypeface(type);
		login.setTypeface(type);
		createaccount.setTypeface(type);
		createaccount.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent i=new Intent("com.liquamart.Registration");
				i.putExtra("login", "email");
				startActivity(i);
				finish();
			}
		});
		
		login.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(email.getText().toString().length()==0)
					Toast.makeText(getApplicationContext(), "Please enter Email Address", Toast.LENGTH_LONG).show();
				else if (!isValidEmailAddress(email.getText().toString()))
					Toast.makeText(getApplicationContext(),
							"Please enter Correct Email Address", Toast.LENGTH_LONG)
							.show();
				else if(password.getText().toString().length()==0)
					Toast.makeText(getApplicationContext(), "Please enter Password", Toast.LENGTH_LONG).show();
				else{
					isInternetPresent = cd.isConnectingToInternet();
	                if (isInternetPresent) {
	                	new UserLogin().execute();
	                }
	                else
	                {
	                	showAlertDialog(LoginwithEmail.this, "No Internet Connection",
	                            "You don't have internet connection.", false);
	                }
				}
				
			}
		});
	}
	public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
 }
	
	public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
 
        // Setting Dialog Title
        alertDialog.setTitle(title);
 
        // Setting Dialog Message
        alertDialog.setMessage(message);
         
        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.app_icon : R.drawable.app_icon);
 
        // Showing Alert Message
        alertDialog.show();
    }
	
	private class UserLogin extends AsyncTask<String, String, String> {

		Dialog dialog;
		CircularProgressView progressView;
		TextView progress_text;
		String accessToken, userid, loginurl,result,name,contact,state;
		int errorcode;
		Boolean proceed=true;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// custom dialog
			dialog = new Dialog(LoginwithEmail.this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.custom_dialog);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			progressView = (CircularProgressView) dialog
					.findViewById(R.id.progressView);
			progress_text = (TextView) dialog.findViewById(R.id.progresstext);
			progress_text.setText("Logging in..");
			progress_text.setTypeface(type);
			com.Material.Utilities.startAnimationThreadStuff(500, progressView);
			progressView.setIndeterminate(true);
			dialog.show();
			loginurl = "http://app.liquamart.com/api/users/login";
			Log.i("loginurl", loginurl);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg) {
			// do above Server call here
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(loginurl);

			try {
				// Add your data
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						2);
				// Add all the variables
				Log.i("result", email
						.getText().toString());
				nameValuePairs.add(new BasicNameValuePair("email", email
						.getText().toString()));
			    nameValuePairs.add(new BasicNameValuePair("password", password
						.getText().toString()));

				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();

				InputStream in = entity.getContent();

				BufferedReader br = new BufferedReader(new InputStreamReader(
						in, "UTF-8"), 8);

				StringBuilder sb = new StringBuilder();
				String line = null;

				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");

				}
				result = sb.toString();
				 try {
				 JSONObject rs = new JSONObject(result);
				 accessToken = rs.getString("id");
				 userid = rs.getString("userId");
				 name = rs.getString("name");
				 contact = rs.getString("contact");
				 state = rs.getString("state");
				 Log.d("accessToken", accessToken);
				
				 } catch (JSONException e) {
				 // TODO Auto-generated catch block
				 e.printStackTrace();
				 }
				 try {
						JSONObject data=new JSONObject(result);
						JSONObject error=data.getJSONObject("error");
						errorcode=error.getInt("status");
						proceed=false;
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						proceed=true;
						e.printStackTrace();
					}

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// process message
			// Toast.makeText(getApplicationContext(), result,
			// Toast.LENGTH_LONG).show();
			Log.i("result", result);
			// progressView.setVisibility(View.GONE);
			dialog.dismiss();
			Log.i("result", result);
			if(proceed==true)
			{
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(LoginwithEmail.this);
			SharedPreferences.Editor editor = preferences.edit();
			editor.putString("accessToken", accessToken);
			editor.putString("userid", userid);
			editor.putString("name", name);
			editor.putString("email", email.getText().toString());
			editor.putString("progress", "main");
			editor.putString("logintype", "liqua");
			editor.putString("state", state);
			editor.putString("phone", contact);
			editor.apply();
			Intent i = new Intent("com.liquamart.Categories");
			startActivity(i);
			finish();
			}
			else if(proceed==false)
				Toast.makeText(getApplicationContext(), "Login failed. Please check your mail or password ", Toast.LENGTH_LONG).show();

			// editor.putString("photourl", personPhotoUrl+"0");
		}
	}
	

}