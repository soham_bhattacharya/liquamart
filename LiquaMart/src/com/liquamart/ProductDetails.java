package com.liquamart;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.Material.CircularProgressView;
import com.Material.ConnectionDetector;
import com.categories.tabs.MyAdapter;
import com.categories.tabs.MyAdapterProductDetails;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.facebook.Session;
import com.facebook.UiLifecycleHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;

public class ProductDetails extends ActionBarActivity implements
		ConnectionCallbacks, OnConnectionFailedListener {

	// First We Declare Titles And Icons For Our Navigation Drawer List View
	// This Icons And Titles Are holded in an Array as you can see

	String TITLES[] = { "Home", "My Products", "Favorites",
			"Order Replacement", "FAQ's", "Contact Support",
			"Profile Settings", "Logout" };
	String TITLES2[] = { "Home", "My Products", "Favorites",
			"Order Replacement", "FAQ's", "Contact Support",
			"Profile Settings" };
	int ICONS[] = { R.drawable.icon_home, R.drawable.icon_products,
			R.drawable.icon_favourite, R.drawable.icon_replacement,
			R.drawable.icon_faqs, R.drawable.icon_contact,
			R.drawable.icon_profileset, R.drawable.icon_logout };
	String NAME, EMAIL, PHOTO;
	private SliderLayout mDemoSlider;
	private Toolbar toolbar; // Declaring the Toolbar Object
	RecyclerView mRecyclerView; // Declaring RecyclerView
	RecyclerView.Adapter mAdapter; // Declaring Adapter For Recycler View
	RecyclerView.LayoutManager mLayoutManager; // Declaring Layout Manager as a
												// linear layout manager
	public static DrawerLayout Drawer; // Declaring DrawerLayout
	ActionBarDrawerToggle mDrawerToggle; // Declaring Action Bar Drawer Toggle

	private static GoogleApiClient mGoogleApiClient;
	private UiLifecycleHelper uiHelper;
	private static final int RC_SIGN_IN = 0;
	private boolean mSignInClicked;
	private ConnectionResult mConnectionResult;
	// private SignInButton btnSignIn;
	private boolean mIntentInProgress;
	public static Activity activity = null;
	Typeface type;
	TextView product_name, product_descrp;
	Button pdf, video, ebay, amazon, liquamart;
	String productid, producturl, name, description, pdflink, videolink,
			ebaylink, amazonlink, liqualink;
	String images[];
	ImageView fav;
	Boolean isInternetPresent = false;
	ConnectionDetector cd;
	Boolean addedtofav=false;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.productdetails);
		cd = new ConnectionDetector(getApplicationContext());
		activity = this;
		type = Typeface.createFromAsset(getAssets(),
				"fonts/sanfracisco_regular.ttf");
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(ProductDetails.this);
		String NAME = preferences.getString("name", "");
		String EMAIL = preferences.getString("email", "");
		String PHOTO = preferences.getString("photourl", "");
		String logintype = preferences.getString("logintype", "");
		Bundle extras = getIntent().getExtras();
		productid = extras.getString("id", "");

		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(Plus.API, Plus.PlusOptions.builder().build())
				.addScope(Plus.SCOPE_PLUS_LOGIN).build();

		/*
		 * Assinging the toolbar object ot the view and setting the the Action
		 * bar to our toolbar
		 */
		toolbar = (Toolbar) findViewById(R.id.tool_bar);
		toolbar.setTitle("LiquaMart");
		setSupportActionBar(toolbar);

		mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning
																		// the
																		// RecyclerView
																		// Object
																		// to
																		// the
																		// xml
																		// View

		mRecyclerView.setHasFixedSize(true); // Letting the system know that the
												// list objects are of fixed
												// size

		if (logintype.equals("facebook") || logintype.equals("google")
				|| logintype.equals("liqua")) {
			mAdapter = new MyAdapter(getApplicationContext(), activity, TITLES,
					ICONS, NAME, EMAIL, PHOTO);
		} else {
			mAdapter = new MyAdapter(getApplicationContext(), activity,
					TITLES2, ICONS, NAME, EMAIL, PHOTO);
		}
		mRecyclerView.setAdapter(mAdapter); // Setting the adapter to
											// RecyclerView

		mLayoutManager = new LinearLayoutManager(this); // Creating a layout
														// Manager

		mRecyclerView.setLayoutManager(mLayoutManager); // Setting the layout
														// Manager

		Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout); // Drawer
																	// object
																	// Assigned
																	// to the
																	// view
		mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, toolbar,
				R.string.openDrawer, R.string.closeDrawer) {

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				// code here will execute once the drawer is opened( As I dont
				// want anything happened whe drawer is
				// open I am not going to put anything here)
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				// Code here will execute once drawer is closed
			}

		}; // Drawer Toggle Object Made
		Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the
													// Drawer toggle
		mDrawerToggle.syncState(); // Finally we set the drawer toggle sync
									// State

		product_name = (TextView) findViewById(R.id.product_name);
		product_descrp = (TextView) findViewById(R.id.product_description);
		pdf = (Button) findViewById(R.id.pdf);
		video = (Button) findViewById(R.id.video);
		ebay = (Button) findViewById(R.id.ebay);
		amazon = (Button) findViewById(R.id.amazon);
		fav=(ImageView) findViewById(R.id.fav);
		liquamart = (Button) findViewById(R.id.liquamart);
		product_name.setTypeface(type);
		product_descrp.setTypeface(type);
		pdf.setTypeface(type);
		video.setTypeface(type);
		ebay.setTypeface(type);
		amazon.setTypeface(type);
		liquamart.setTypeface(type);

		mDemoSlider = (SliderLayout) findViewById(R.id.slider);
		isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
		new GetProduct().execute();
        }
        else
        {
        	showAlertDialog(ProductDetails.this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
		
		fav.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				isInternetPresent = cd.isConnectingToInternet();
		        if (isInternetPresent) {
				if(addedtofav==false)
					new Addfavorite().execute();
				else
					Toast.makeText(getApplicationContext(), "Already added to Favorites", Toast.LENGTH_LONG).show();
			}
			else
			{
				showAlertDialog(ProductDetails.this, "No Internet Connection",
	                    "You don't have internet connection.", false);
			}
			}
		});
		

	}
	public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
 
        // Setting Dialog Title
        alertDialog.setTitle(title);
 
        // Setting Dialog Message
        alertDialog.setMessage(message);
         
        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.app_icon : R.drawable.app_icon);
 
        // Showing Alert Message
        alertDialog.show();
    }
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		if (Drawer.isDrawerOpen(Gravity.LEFT)) {
			Drawer.closeDrawer(Gravity.LEFT);
		} else {
			finish();
			super.onBackPressed();
		}

	}

	public static void callFacebookLogout(Context context) {

		Session session = Session.getActiveSession();
		if (session != null) {

			if (!session.isClosed()) {
				session.closeAndClearTokenInformation();
				// Toast.makeText(context, "Logged out from facebook",
				// Toast.LENGTH_SHORT).show();
				// clear your preferences if saved
			}

		} else {
			session = new Session(context);
			Session.setActiveSession(session);
			session.closeAndClearTokenInformation();
			// clear your preferences if saved
		}
	}

	public static void signOutFromGplus() {
		if (mGoogleApiClient.isConnected()) {

			Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
			mGoogleApiClient.disconnect();
			mGoogleApiClient.connect();
			// Toast.makeText(activity, "Logged out from google",
			// Toast.LENGTH_SHORT).show();
			activity.finish();
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub
		if (!result.hasResolution()) {
			GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
					0).show();
			return;
		}

		if (!mIntentInProgress) {
			// Store the ConnectionResult for later usage
			mConnectionResult = result;

			if (mSignInClicked) {
				// The user has already clicked 'sign-in' so we attempt to
				// resolve all
				// errors until the user is signed in, or they cancel.
				resolveSignInError();
			}
		}

	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		mSignInClicked = false;
		// Toast.makeText(this, "User is connected!",
		// Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		mGoogleApiClient.connect();

	}

	private void resolveSignInError() {
		if (mConnectionResult.hasResolution()) {
			try {
				mIntentInProgress = true;
				mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
			} catch (SendIntentException e) {
				mIntentInProgress = false;
				mGoogleApiClient.connect();
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
		if (requestCode == RC_SIGN_IN) {
			if (resultCode != RESULT_OK) {
				mSignInClicked = false;
			}

			mIntentInProgress = false;

			if (!mGoogleApiClient.isConnecting()) {
				mGoogleApiClient.connect();
			}
		}
	}

	protected void onStop() {
		super.onStop();
		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
	}

	protected void onStart() {
		super.onStart();
		mGoogleApiClient.connect();
	}

	private class GetProduct extends AsyncTask<String, String, String> {
		Dialog dialog;
		CircularProgressView progressView;
		TextView progress_text;
		String result;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// custom dialog
			dialog = new Dialog(activity);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.custom_dialog);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			progressView = (CircularProgressView) dialog
					.findViewById(R.id.progressView);
			progress_text = (TextView) dialog.findViewById(R.id.progresstext);
			progress_text.setText("Loading..");
			progress_text.setTypeface(type);
			com.Material.Utilities.startAnimationThreadStuff(500, progressView);
			progressView.setIndeterminate(true);
			dialog.show();
			producturl = "http://app.liquamart.com/api/products/"+ productid;
			Log.i("myproductsurl", producturl);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg) {
			// do above Server call here
			result = getJsonDataget(producturl);

			Log.i("result", result);
			try {
				JSONObject product = new JSONObject(result);
				name = product.getString("name");
				JSONArray imgurls = product.getJSONArray("imgUrls");
				images = new String[imgurls.length()];
				for (int i = 0; i < imgurls.length(); i++) {
					images[i] = imgurls.getString(i);
					Log.i("image", images[i]);
				}
				
				JSONObject link = product.getJSONObject("links");
				pdflink = link.getString("pdf");
				videolink = link.getString("YT");
				ebaylink = link.getString("ebay");
				amazonlink = link.getString("amazon");
				liqualink = link.getString("liqua");
				description = product.getString("description");
				

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// process message
			// Toast.makeText(getApplicationContext(), result,
			// Toast.LENGTH_LONG).show();

			Log.i("result", result);
			product_name.setText(name);
			product_name.setVisibility(View.VISIBLE);
			product_descrp.setText(description);
			product_descrp.setVisibility(View.VISIBLE);
			if (video.getText().toString() != null || !video.getText().toString().isEmpty() )
				video.setVisibility(View.VISIBLE);
			video.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Toast.makeText(getApplicationContext(), videolink,
							Toast.LENGTH_LONG).show();
				}
			});
			if (pdf.getText().toString() != null || !pdf.getText().toString().isEmpty())
				pdf.setVisibility(View.VISIBLE);
			pdf.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Toast.makeText(getApplicationContext(), pdflink,
							Toast.LENGTH_LONG).show();
				}
			});
			if (ebay.getText().toString() != null || !ebay.getText().toString().isEmpty())
				ebay.setVisibility(View.VISIBLE);
			ebay.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Toast.makeText(getApplicationContext(), ebaylink,
							Toast.LENGTH_LONG).show();
				}
			});
			if (amazon.getText().toString() != null || !amazon.getText().toString().isEmpty())
				amazon.setVisibility(View.VISIBLE);
			amazon.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Toast.makeText(getApplicationContext(), amazonlink,
							Toast.LENGTH_LONG).show();
				}
			});
			if (liquamart.getText().toString() != null || !liquamart.getText().toString().isEmpty())
				liquamart.setVisibility(View.VISIBLE);
			liquamart.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Toast.makeText(getApplicationContext(), liqualink,
							Toast.LENGTH_LONG).show();
				}
			});
			HashMap<String, String> url_maps = new HashMap<String, String>();
			for(int j=0;j<images.length;j++)
			{
				url_maps.put("image", images[j]);
			}
			
			for (String name : url_maps.keySet()) {
				DefaultSliderView textSliderView = new DefaultSliderView(ProductDetails.this);
				// initialize a SliderLayout
				textSliderView.image(url_maps.get(name)).setScaleType(
						BaseSliderView.ScaleType.FitCenterCrop);
				mDemoSlider.addSlider(textSliderView);
			}
			mDemoSlider
					.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
			if(images.length==1 || images.length==0){
			mDemoSlider.setCustomAnimation(new DescriptionAnimation());
			mDemoSlider.setDuration(350000);
			}
			mDemoSlider.setVisibility(View.VISIBLE);
			dialog.dismiss();
			

		}
	}
	
	private class Addfavorite extends AsyncTask<String, String, String> {

		Dialog dialog;
		CircularProgressView progressView;
		TextView progress_text;
		String accessToken, userid, favurl,result;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// custom dialog
			dialog = new Dialog(ProductDetails.this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.custom_dialog);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			progressView = (CircularProgressView) dialog
					.findViewById(R.id.progressView);
			progress_text = (TextView) dialog.findViewById(R.id.progresstext);
			progress_text.setText("Adding to Favorites..");
			progress_text.setTypeface(type);
			com.Material.Utilities.startAnimationThreadStuff(500, progressView);
			progressView.setIndeterminate(true);
			dialog.show();
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(ProductDetails.this);
			accessToken = preferences.getString("accessToken", "");
			userid = preferences.getString("userid", "");
			Log.i("id", userid);
			favurl = "http://app.liquamart.com/api/favourites?"
					+ "access_token=" + accessToken;
			Log.i("favurl", favurl);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg) {
			// do above Server call here
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(favurl);

			try {
				// Add your data
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						2);
				// Add all the variables
				nameValuePairs.add(new BasicNameValuePair("productId", productid));
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();

				InputStream in = entity.getContent();

				BufferedReader br = new BufferedReader(new InputStreamReader(
						in, "UTF-8"), 8);

				StringBuilder sb = new StringBuilder();
				String line = null;

				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");

				}
				result = sb.toString();
				// try {
				// JSONObject rs = new JSONObject(result);
				// accessToken = rs.getString("accessToken");
				// userid = rs.getString("id");
				// Log.d("accessToken", accessToken);
				//
				// } catch (JSONException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			Log.i("result", result);
			dialog.dismiss();
			addedtofav=true;
		}
	}

	public String getJsonDataget(String url) {

		StringBuilder builder = new StringBuilder();

		HttpClient client = new DefaultHttpClient();

		HttpGet httpGet = new HttpGet(url);

		try {

			HttpResponse response = client.execute(httpGet);

			StatusLine statusLine = response.getStatusLine();

			int statusCode = statusLine.getStatusCode();

			if (statusCode == 200) {

				HttpEntity entity = response.getEntity();

				InputStream content = entity.getContent();

				BufferedReader reader = new BufferedReader(

				new InputStreamReader(content));

				String line;

				while ((line = reader.readLine()) != null) {

					builder.append(line);

				}

			} else {

				// System.out.print("condition not true");

				// Log.e(ParseJSON.class.toString(),
				// "Failed to download file");

			}

		} catch (ClientProtocolException e) {

			e.printStackTrace();

		} catch (IOException e) {
			e.printStackTrace();

		}

		return builder.toString();

	}

}