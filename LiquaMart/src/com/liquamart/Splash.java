package com.liquamart;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.widget.ImageView;

import com.Material.FasterAnimationsContainer;
import com.facebook.Session;

@SuppressWarnings("deprecation")
public class Splash extends ActionBarActivity {
	ImageView frame;
	AnimationDrawable ad;
	ActionBar ab;
	FasterAnimationsContainer mFasterAnimationsContainer;
	private static final int[] IMAGE_RESOURCES = { R.drawable.liquamart0052,
			R.drawable.liquamart0054, R.drawable.liquamart0056,
			R.drawable.liquamart0058, R.drawable.liquamart0060,
			R.drawable.liquamart0062, R.drawable.liquamart0064,
			R.drawable.liquamart0066, R.drawable.liquamart0068,
			R.drawable.liquamart0070, R.drawable.liquamart0072,
			 R.drawable.liquamart0074,
			 R.drawable.liquamart0076,
			 R.drawable.liquamart0078,
			 R.drawable.liquamart0080,
			 R.drawable.liquamart0082,
		 R.drawable.liquamart0084,
			R.drawable.liquamart0086,
			R.drawable.liquamart0088,
			R.drawable.liquamart0090,
			R.drawable.liquamart0092,
			R.drawable.liquamart0094,
			R.drawable.liquamart0096,
			R.drawable.liquamart0098,
			R.drawable.liquamart00100,
			R.drawable.liquamart00102,
			R.drawable.liquamart00104,
			R.drawable.liquamart00106,
			R.drawable.liquamart00108,
			R.drawable.liquamart00110,
			R.drawable.liquamart00112,
			R.drawable.liquamart00114,
			R.drawable.liquamart00116,
			R.drawable.liquamart00118, 
			R.drawable.liquamart00120, 
			R.drawable.liquamart00122, 
			R.drawable.liquamart00124, 
			R.drawable.liquamart00126,
			R.drawable.liquamart00128, 
			R.drawable.liquamart00130, 
			R.drawable.liquamart00132, 
			R.drawable.liquamart00134, 
			R.drawable.liquamart00136, 
			R.drawable.liquamart00138, 
			R.drawable.liquamart00140, 
			R.drawable.liquamart00142, 
			R.drawable.liquamart00144, 
			R.drawable.liquamart00146, 
			R.drawable.liquamart00148, 
			R.drawable.liquamart00150, 
			R.drawable.liquamart00152, 
			R.drawable.liquamart00154, 
			R.drawable.liquamart00156, 
			R.drawable.liquamart00158, 
			R.drawable.liquamart00160, 
			R.drawable.liquamart00162, 
			R.drawable.liquamart00164, 
			R.drawable.liquamart00166, 
			R.drawable.liquamart00168, 
			R.drawable.liquamart00170, 
			R.drawable.liquamart00172, 
			R.drawable.liquamart00174, 
			R.drawable.liquamart00176  };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		ab = getSupportActionBar();
		ab.hide();

		frame = (ImageView) findViewById(R.id.frame_image);
		mFasterAnimationsContainer = FasterAnimationsContainer
				.getInstance(frame);
		mFasterAnimationsContainer.addAllFrames(IMAGE_RESOURCES, 100);
		mFasterAnimationsContainer.start();
		// frame.setBackgroundResource(R.drawable.frame_animation);
		// ad = (AnimationDrawable) frame.getBackground();
		// ad.start();
		new Thread() {
			public void run() {
				try {

					// Thread.sleep(4500);
					Thread.sleep(5250);
					finish();
					Intent i = new Intent("com.liquamart.Login");
					startActivity(i);

				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}.start();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mFasterAnimationsContainer.stop();
	}

}