package com.liquamart;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.Material.CircularProgressView;
import com.Material.ConnectionDetector;
import com.Material.MaterialEditText;
import com.Material.MaterialSpinner;
import com.categories.tabs.MyAdapterContact;
import com.categories.tabs.MyAdapterSettings;
import com.facebook.Session;
import com.facebook.UiLifecycleHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;

public class ProfileSettings extends ActionBarActivity implements
		ConnectionCallbacks, OnConnectionFailedListener {

	// First We Declare Titles And Icons For Our Navigation Drawer List View
	// This Icons And Titles Are holded in an Array as you can see

	String TITLES[] = { "Home", "My Products", "Favorites",
			"Order Replacement", "FAQ's", "Contact Support",
			"Profile Settings", "Logout" };
	int ICONS[] = { R.drawable.icon_home, R.drawable.icon_products,
			R.drawable.icon_favourite, R.drawable.icon_replacement,
			R.drawable.icon_faqs, R.drawable.icon_contact,
			R.drawable.icon_bprofileset, R.drawable.icon_logout };
	int position = 0;

	String NAME, EMAIL, PHOTO;
	private Toolbar toolbar; // Declaring the Toolbar Object
	RecyclerView mRecyclerView; // Declaring RecyclerView
	RecyclerView.Adapter mAdapter; // Declaring Adapter For Recycler View
	RecyclerView.LayoutManager mLayoutManager; // Declaring Layout Manager as a
												// linear layout manager
	public static DrawerLayout Drawer; // Declaring DrawerLayout
	ActionBarDrawerToggle mDrawerToggle; // Declaring Action Bar Drawer Toggle

	private static GoogleApiClient mGoogleApiClient;
	private UiLifecycleHelper uiHelper;
	private static final int RC_SIGN_IN = 0;
	private boolean mSignInClicked;
	private ConnectionResult mConnectionResult;
	// private SignInButton btnSignIn;
	private boolean mIntentInProgress;
	public static Activity activity = null;
	Typeface type;
	MaterialSpinner spinner;
	MaterialEditText name, email, phone;
	Button sendmessage;
	Boolean isInternetPresent = false;
	ConnectionDetector cd;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profileset);
		activity = this;
		cd = new ConnectionDetector(getApplicationContext());
		type = Typeface.createFromAsset(getAssets(),
				"fonts/sanfracisco_regular.ttf");
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(ProfileSettings.this);
		String NAME = preferences.getString("name", "");
		final String EMAIL = preferences.getString("email", "");
		String PHOTO = preferences.getString("photourl", "");
		String PHONE = preferences.getString("phone", "");
		String state = preferences.getString("state", "");
		String[] ITEMS = getResources().getStringArray(R.array.states);
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(Plus.API, Plus.PlusOptions.builder().build())
				.addScope(Plus.SCOPE_PLUS_LOGIN).build();

		/*
		 * Assinging the toolbar object ot the view and setting the the Action
		 * bar to our toolbar
		 */
		toolbar = (Toolbar) findViewById(R.id.tool_bar);
		toolbar.setTitle("Update Profile");
		setSupportActionBar(toolbar);

		mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView); // Assigning
																		// the
																		// RecyclerView
																		// Object
																		// to
																		// the
																		// xml
																		// View

		mRecyclerView.setHasFixedSize(true); // Letting the system know that the
												// list objects are of fixed
												// size

		mAdapter = new MyAdapterSettings(getApplicationContext(), activity,
				TITLES, ICONS, NAME, EMAIL, PHOTO); // Creating the Adapter of
													// MyAdapter
													// class(which
													// we are going to see in a
													// bit)
		// And passing the titles,icons,header view name, header view email,
		// and header view profile picture

		mRecyclerView.setAdapter(mAdapter); // Setting the adapter to
											// RecyclerView

		mLayoutManager = new LinearLayoutManager(this); // Creating a layout
														// Manager

		mRecyclerView.setLayoutManager(mLayoutManager); // Setting the layout
														// Manager

		Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout); // Drawer
																	// object
																	// Assigned
																	// to the
																	// view
		mDrawerToggle = new ActionBarDrawerToggle(this, Drawer, toolbar,
				R.string.openDrawer, R.string.closeDrawer) {

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				// code here will execute once the drawer is opened( As I dont
				// want anything happened whe drawer is
				// open I am not going to put anything here)
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				// Code here will execute once drawer is closed
			}

		}; // Drawer Toggle Object Made
		Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the
													// Drawer toggle
		mDrawerToggle.syncState(); // Finally we set the drawer toggle sync
									// State
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, ITEMS);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner = (MaterialSpinner) findViewById(R.id.spinner_states);
		spinner.setAdapter(adapter);
		for (int i = 0; i < ITEMS.length; i++) {
			if (ITEMS[i].equals(state)) {
				position = i;
			}
		}
		try {
			spinner.setSelection(position + 1);
		} catch (Exception e) {

		}
		name = (MaterialEditText) findViewById(R.id.text_name);
		email = (MaterialEditText) findViewById(R.id.text_email);
		phone = (MaterialEditText) findViewById(R.id.text_phone);
		name.setTypeface(type);
		email.setTypeface(type);
		phone.setTypeface(type);
		name.setText(NAME);
		email.setText(EMAIL);
		phone.setText(PHONE);
		sendmessage = (Button) findViewById(R.id.change_settings);
		sendmessage.setTypeface(type);
		sendmessage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (name.getText().length() == 0)
					Toast.makeText(getApplicationContext(),
							"Please enter Name", Toast.LENGTH_LONG).show();
				else if (phone.getText().length() == 0)
					Toast.makeText(getApplicationContext(),
							"Please enter Phone Number", Toast.LENGTH_LONG)
							.show();
				else if (email.getText().length() == 0)
					Toast.makeText(getApplicationContext(),
							"Please enter Email Address", Toast.LENGTH_LONG)
							.show();
				else if (!isValidEmailAddress(email.getText().toString()))
					Toast.makeText(getApplicationContext(),
							"Please enter Correct Email Address", Toast.LENGTH_LONG)
							.show();
				else if (spinner.getSelectedItem().toString().equals("State"))
					Toast.makeText(getApplicationContext(),
							"Please select a state", Toast.LENGTH_LONG).show();
				else {

					isInternetPresent = cd.isConnectingToInternet();
	                if (isInternetPresent) {
					new UserUpdate().execute();
	                }
	                else
	                {
	                	showAlertDialog(ProfileSettings.this, "No Internet Connection",
	                            "You don't have internet connection.", false);
	                }
				}

			}
		});

	}

	public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
 
        // Setting Dialog Title
        alertDialog.setTitle(title);
 
        // Setting Dialog Message
        alertDialog.setMessage(message);
         
        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.app_icon : R.drawable.app_icon);
 
        // Showing Alert Message
        alertDialog.show();
    }
	
	public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
 }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		if (Drawer.isDrawerOpen(Gravity.LEFT)) {
			Drawer.closeDrawer(Gravity.LEFT);
		} else {
			finish();
			super.onBackPressed();
		}

	}

	public static void callFacebookLogout(Context context) {

		Session session = Session.getActiveSession();
		if (session != null) {

			if (!session.isClosed()) {
				session.closeAndClearTokenInformation();
				// Toast.makeText(context, "Logged out from facebook",
				// Toast.LENGTH_SHORT).show();
				// clear your preferences if saved
			}

		} else {
			session = new Session(context);
			Session.setActiveSession(session);
			session.closeAndClearTokenInformation();
			// clear your preferences if saved
		}
	}

	public static void signOutFromGplus() {
		if (mGoogleApiClient.isConnected()) {

			Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
			mGoogleApiClient.disconnect();
			mGoogleApiClient.connect();
			// Toast.makeText(activity, "Logged out from google",
			// Toast.LENGTH_SHORT).show();
			activity.finish();
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub
		if (!result.hasResolution()) {
			GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
					0).show();
			return;
		}

		if (!mIntentInProgress) {
			// Store the ConnectionResult for later usage
			mConnectionResult = result;

			if (mSignInClicked) {
				// The user has already clicked 'sign-in' so we attempt to
				// resolve all
				// errors until the user is signed in, or they cancel.
				resolveSignInError();
			}
		}

	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		mSignInClicked = false;
		// Toast.makeText(this, "User is connected!",
		// Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		mGoogleApiClient.connect();

	}

	private void resolveSignInError() {
		if (mConnectionResult.hasResolution()) {
			try {
				mIntentInProgress = true;
				mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
			} catch (SendIntentException e) {
				mIntentInProgress = false;
				mGoogleApiClient.connect();
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
		if (requestCode == RC_SIGN_IN) {
			if (resultCode != RESULT_OK) {
				mSignInClicked = false;
			}

			mIntentInProgress = false;

			if (!mGoogleApiClient.isConnecting()) {
				mGoogleApiClient.connect();
			}
		}
	}

	protected void onStop() {
		super.onStop();
		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
	}

	protected void onStart() {
		super.onStart();
		mGoogleApiClient.connect();
	}

	private class UserUpdate extends AsyncTask<String, String, String> {

		Dialog dialog;
		CircularProgressView progressView;
		TextView progress_text;
		String accessToken, userid, updateurl, result;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// custom dialog
			dialog = new Dialog(ProfileSettings.this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.custom_dialog);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			progressView = (CircularProgressView) dialog
					.findViewById(R.id.progressView);
			progress_text = (TextView) dialog.findViewById(R.id.progresstext);
			progress_text.setText("Updating Profile...");
			progress_text.setTypeface(type);
			com.Material.Utilities.startAnimationThreadStuff(500, progressView);
			progressView.setIndeterminate(true);
			dialog.show();
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(ProfileSettings.this);
			accessToken = preferences.getString("accessToken", "");
			userid = preferences.getString("userid", "");
			Log.i("id", userid);
			updateurl = "http://app.liquamart.com/api/users/" + userid
					+ "?access_token=" + accessToken;
			Log.i("updateurl", updateurl);
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg) {
			// do above Server call here
			HttpClient httpclient = new DefaultHttpClient();
			HttpPut httppost = new HttpPut(updateurl);

			try {
				// Add your data
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						2);
				// Add all the variables
				nameValuePairs.add(new BasicNameValuePair("email", email
						.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("contact", phone
						.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("state", spinner
						.getSelectedItem().toString()));

				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();

				InputStream in = entity.getContent();

				BufferedReader br = new BufferedReader(new InputStreamReader(
						in, "UTF-8"), 8);

				StringBuilder sb = new StringBuilder();
				String line = null;

				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");

				}
				result = sb.toString();
				// try {
				// JSONObject rs = new JSONObject(result);
				// accessToken = rs.getString("accessToken");
				// userid = rs.getString("id");
				// Log.d("accessToken", accessToken);
				//
				// } catch (JSONException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// process message
			// Toast.makeText(getApplicationContext(), result,
			// Toast.LENGTH_LONG).show();
			Log.i("result", result);
			// progressView.setVisibility(View.GONE);
			dialog.dismiss();
			// Log.i("result", result);
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(ProfileSettings.this);
			SharedPreferences.Editor editor = preferences.edit();
			editor.putString("state", spinner.getSelectedItem().toString());
			editor.putString("phone", phone.getText().toString());
			editor.putString("email", email.getText().toString());
			editor.apply();
			Toast.makeText(getApplicationContext(), "Profile Settings changed",
					Toast.LENGTH_LONG).show();
			finish();

			// editor.putString("photourl", personPhotoUrl+"0");
		}
	}

}