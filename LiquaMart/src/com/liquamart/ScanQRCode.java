package com.liquamart;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import net.sourceforge.zbar.Symbol;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.Material.CircularProgressView;
import com.Material.ConnectionDetector;
import com.Material.MaterialEditText;
import com.dm.zbar.android.scanner.ZBarConstants;
import com.dm.zbar.android.scanner.ZBarScannerActivity;

public class ScanQRCode extends Activity {

	private ImageButton scan;
	private static final int ZBAR_SCANNER_REQUEST = 0;
	private static final int ZBAR_QR_SCANNER_REQUEST = 1;
	TextView scan_msg, boughtfrom;
	MaterialEditText qrcode, productname;
	Button ebay, amazon, liquamart, registerproduct;
	String resultqr = "", site = "empty",id;
	int status = 0;
	Boolean isInternetPresent = false;
	ConnectionDetector cd;

	Typeface type;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scanqrcode);
		cd = new ConnectionDetector(getApplicationContext());
		type = Typeface.createFromAsset(getAssets(),
				"fonts/sanfracisco_regular.ttf");
		scan = (ImageButton) findViewById(R.id.camera_image);
		scan_msg = (TextView) findViewById(R.id.scan_msg);
		boughtfrom = (TextView) findViewById(R.id.text_boughtfrom);
		qrcode = (MaterialEditText) findViewById(R.id.text_qrcode);
		productname = (MaterialEditText) findViewById(R.id.text_product_name);
		productname.setFocusable(false);
		qrcode.setFocusable(false);
		ebay = (Button) findViewById(R.id.Ebay);
		amazon = (Button) findViewById(R.id.amazon);
		registerproduct = (Button) findViewById(R.id.registerproduct);
		liquamart = (Button) findViewById(R.id.liquamart);
		scan_msg.setTypeface(type);
		boughtfrom.setTypeface(type);
		qrcode.setTypeface(type);
		productname.setTypeface(type);
		ebay.setTypeface(type);
		amazon.setTypeface(type);
		liquamart.setTypeface(type);
		registerproduct.setTypeface(type);
		scan.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
				Toast.makeText(
						getApplicationContext(),
						"Hold camera in front of QR Code. It will be scanned automatically.",
						Toast.LENGTH_LONG).show();
				launchQRScanner();
                }
                else
                {
                	showAlertDialog(ScanQRCode.this, "No Internet Connection",
                            "You don't have internet connection.", false);
                }
			}
		});

		registerproduct.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (resultqr.equals("") || status == 500)
					Toast.makeText(getApplicationContext(),
							"Please scan the QR Code", Toast.LENGTH_LONG)
							.show();
				else if (site.equals("empty")) {
					Toast.makeText(getApplicationContext(),
							"Please select bought from", Toast.LENGTH_LONG)
							.show();
				} else if (status == 200) {
					isInternetPresent = cd.isConnectingToInternet();
	                if (isInternetPresent) {
					new PurchaseProduct().execute();
	                }
	                else
	                {
	                	showAlertDialog(ScanQRCode.this, "No Internet Connection",
	                            "You don't have internet connection.", false);
	                }
				}
			}
		});

		ebay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				site = "ebay";
//				ebay.setBackgroundColor(Color.parseColor("#55AAFF"));
//				liquamart.setBackgroundColor(Color.parseColor("#FFFFFF"));
//				amazon.setBackgroundColor(Color.parseColor("#FFFFFF"));
				ebay.setTextColor(Color.parseColor("#305FAD"));
				ebay.setTypeface(null, Typeface.BOLD);
				amazon.setTypeface(null, Typeface.NORMAL);
				liquamart.setTypeface(null, Typeface.NORMAL);
			}
		});
		amazon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				site = "amazon";
//				amazon.setBackgroundColor(Color.parseColor("#55AAFF"));
//				liquamart.setBackgroundColor(Color.parseColor("#FFFFFF"));
//				ebay.setBackgroundColor(Color.parseColor("#FFFFFF"));
				amazon.setTextColor(Color.parseColor("#305FAD"));
				amazon.setTypeface(null, Typeface.BOLD);
				ebay.setTypeface(null, Typeface.NORMAL);
				liquamart.setTypeface(null, Typeface.NORMAL);
			}
		});
		liquamart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				site = "liquamart";
//				liquamart.setBackgroundColor(Color.parseColor("#55AAFF"));
//				ebay.setBackgroundColor(Color.parseColor("#FFFFFF"));
//				amazon.setBackgroundColor(Color.parseColor("#FFFFFF"));
				liquamart.setTextColor(Color.parseColor("#305FAD"));
				liquamart.setTypeface(null, Typeface.BOLD);
				ebay.setTypeface(null, Typeface.NORMAL);
				amazon.setTypeface(null, Typeface.NORMAL);
			}
		});

	}

	public void launchScanner(View v) {
		if (isCameraAvailable()) {
			Intent intent = new Intent(this, ZBarScannerActivity.class);
			startActivityForResult(intent, ZBAR_SCANNER_REQUEST);

		} else {
			Toast.makeText(this, "Rear Facing Camera Unavailable",
					Toast.LENGTH_SHORT).show();
		}
	}

	public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
 
        // Setting Dialog Title
        alertDialog.setTitle(title);
 
        // Setting Dialog Message
        alertDialog.setMessage(message);
         
        // Setting alert dialog icon
        alertDialog.setIcon((status) ? R.drawable.app_icon : R.drawable.app_icon);
 
        // Showing Alert Message
        alertDialog.show();
    }
	
	public void launchQRScanner() {
		if (isCameraAvailable()) {
			Intent intent = new Intent(this, ZBarScannerActivity.class);
			intent.putExtra(ZBarConstants.SCAN_MODES,
					new int[] { Symbol.QRCODE });
			startActivityForResult(intent, ZBAR_SCANNER_REQUEST);
		} else {
			Toast.makeText(this, "Rear Facing Camera Unavailable",
					Toast.LENGTH_SHORT).show();
		}
	}

	public boolean isCameraAvailable() {
		PackageManager pm = getPackageManager();
		return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case ZBAR_SCANNER_REQUEST:
		case ZBAR_QR_SCANNER_REQUEST:
			if (resultCode == RESULT_OK) {
				resultqr = data.getStringExtra(ZBarConstants.SCAN_RESULT);

				Log.i("qrcode", resultqr);
				new QRCodeCheck().execute();

			} else if (resultCode == RESULT_CANCELED && data != null) {
				String error = data.getStringExtra(ZBarConstants.ERROR_INFO);
				if (!TextUtils.isEmpty(error)) {
					Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
				}
			}
			break;
		}

	}

	private class QRCodeCheck extends AsyncTask<String, String, String> {
		Dialog dialog;
		CircularProgressView progressView;
		TextView progress_text;
		String result, name, code;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// custom dialog
			dialog = new Dialog(ScanQRCode.this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.custom_dialog);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			progressView = (CircularProgressView) dialog
					.findViewById(R.id.progressView);
			progress_text = (TextView) dialog.findViewById(R.id.progresstext);
			progress_text.setText("Getting information..");
			progress_text.setTypeface(type);
			com.Material.Utilities.startAnimationThreadStuff(500, progressView);
			progressView.setIndeterminate(true);
			dialog.show();
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg) {
			// do above Server call here
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost("http://app.liquamart.com/api/qr");

			try {
				// Add your data
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						2);
				nameValuePairs.add(new BasicNameValuePair("qr", resultqr));
				// httppost.setHeader(HTTP.CONTENT_TYPE,
				// "application/x-www-form-urlencoded;charset=UTF-8");
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();

				InputStream in = entity.getContent();

				BufferedReader br = new BufferedReader(new InputStreamReader(
						in, "UTF-8"), 8);

				StringBuilder sb = new StringBuilder();
				String line = null;

				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");

				}
				result = sb.toString();
				try {
					JSONObject rs = new JSONObject(result);
					status = rs.getInt("status");
					name = rs.getString("name");
					id = rs.getString("id");
					code = rs.getString("qrcode");

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// process message
			// Toast.makeText(getApplicationContext(), result,
			// Toast.LENGTH_LONG).show();
			dialog.dismiss();
			if (status == 500) {
				Toast.makeText(getApplicationContext(),
						"QR Code is not valid. Please try again.",
						Toast.LENGTH_LONG).show();
			} else if (status == 200) {
				productname.setText(name);
				qrcode.setText(code);
				Log.i("result", result);
			}

		}
	}

	private class PurchaseProduct extends AsyncTask<String, String, String> {
		Dialog dialog;
		CircularProgressView progressView;
		TextView progress_text;
		String result, accessToken, code;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			// custom dialog
			dialog = new Dialog(ScanQRCode.this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.custom_dialog);
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
			progressView = (CircularProgressView) dialog
					.findViewById(R.id.progressView);
			progress_text = (TextView) dialog.findViewById(R.id.progresstext);
			progress_text.setText("Registering product..");
			progress_text.setTypeface(type);
			com.Material.Utilities.startAnimationThreadStuff(500, progressView);
			progressView.setIndeterminate(true);
			dialog.show();
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(ScanQRCode.this);
			accessToken = preferences.getString("accessToken", "");
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... arg) {
			// do above Server call here
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
					"http://app.liquamart.com/api/purchases?access_token="
							+ accessToken);

			try {
				// Add your data
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						2);
				nameValuePairs.add(new BasicNameValuePair("site", "amazon"));
				nameValuePairs.add(new BasicNameValuePair("productId", id));
				// httppost.setHeader(HTTP.CONTENT_TYPE,
				// "application/x-www-form-urlencoded;charset=UTF-8");
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();

				InputStream in = entity.getContent();

				BufferedReader br = new BufferedReader(new InputStreamReader(
						in, "UTF-8"), 8);

				StringBuilder sb = new StringBuilder();
				String line = null;

				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");

				}
				result = sb.toString();
				// try {
				// JSONObject rs = new JSONObject(result);
				// name = rs.getString("name");
				// id=rs.getInt("id");
				// code=rs.getString("qrcode");
				// status=rs.getInt("status");
				// } catch (JSONException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
			} catch (IOException e) {
				// TODO Auto-generated catch block
			}
			return result;
		}

		@Override
		protected void onPostExecute(String result) {
			// process message
			// Toast.makeText(getApplicationContext(), result,
			// Toast.LENGTH_LONG).show();
			dialog.dismiss();
			Log.i("result", result);
			Toast.makeText(getApplicationContext(),
					"Product registered successfully.", Toast.LENGTH_LONG)
					.show();
			Intent i = new Intent(ScanQRCode.this, Categories.class);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
					| Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(i);

			finish();

		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}
}
